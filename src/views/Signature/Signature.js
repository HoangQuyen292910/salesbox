import React from 'react';
import { connect } from 'react-redux';
import SignatureComponent from '../../components/Signature';
export const Signature = (props) => {
  return <SignatureComponent />;
};

const mapStateToProps = (state) => ({});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(Signature);
