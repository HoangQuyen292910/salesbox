import React, { useState } from 'react';
import { connect } from 'react-redux';
import { Icon } from 'semantic-ui-react';
import style from './Signature.css';

export const CollapsibleBlock = (props) => {
  const { title, children } = props;

  const [open, setOpen] = useState(false);
  return (
    <div>
      <div onClick={() => setOpen(!open)} className={style.collapsibleHeader} style={{ color: open ? '#008df2' : '' }}>
        {title}
        {open ? <Icon name="angle up" /> : <Icon name="angle down" />}
      </div>
      <div style={{ display: open ? 'block' : 'none', paddingLeft: 16 }}>{children}</div>
    </div>
  );
};

const mapStateToProps = (state) => ({});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(CollapsibleBlock);
