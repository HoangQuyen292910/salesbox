import React, { useState, useEffect, useRef, useCallback } from 'react';
import { connect } from 'react-redux';
import ModalCommon from '../ModalCommon/ModalCommon';
import _l from 'lib/i18n';
import ReactCrop from 'react-image-crop';
import { BLOCKTYPE_SIGNATURE } from '../../Constants';

export const ModalCropPhoto = (props) => {
  const { rootImg, onClose, onDone, visible, imageHeader, uploadImgHeight, uploadImgWidth } = props;

  const imgRef = useRef(null);
  const previewCanvasRef = useRef(null);

  const [crop, setCrop] = useState({ unit: 'px', width: uploadImgWidth, height: uploadImgHeight});
  const [completedCrop, setCompletedCrop] = useState(null);

  useEffect(() => {
    if(uploadImgHeight && uploadImgWidth) setCrop({ unit: 'px', width: uploadImgWidth, height: uploadImgHeight})
  }, [uploadImgHeight, uploadImgWidth])
  useEffect(() => {
    if (!completedCrop || !previewCanvasRef.current || !imgRef.current) {
      return;
    }

    const image = imgRef.current;
    const canvas = previewCanvasRef.current;
    const crop = completedCrop;

    const scaleX = image.naturalWidth / image.width;
    const scaleY = image.naturalHeight / image.height;
    const ctx = canvas.getContext('2d');
    const pixelRatio = window.devicePixelRatio;

    canvas.width = crop.width * pixelRatio;
    canvas.height = crop.height * pixelRatio;

    ctx.setTransform(pixelRatio, 0, 0, pixelRatio, 0, 0);
    ctx.imageSmoothingQuality = 'high';

    ctx.drawImage(
      image,
      crop.x * scaleX,
      crop.y * scaleY,
      crop.width * scaleX,
      crop.height * scaleY,
      0,
      0,
      crop.width,
      crop.height
    );
  }, [completedCrop]);

  const onLoad = useCallback((img) => {
    imgRef.current = img;
  }, []);

  return (
    <ModalCommon
      closeOnDimmerClick={false}
      title={_l`Crop photo`}
      onClose={onClose}
      centered={true}
      scrolling={true}
      size="small"
      paddingAsHeader
      onDone={() => onDone(previewCanvasRef.current.toDataURL('image/png'))}
      visible={visible}
    >
      <div style={{ display: 'flex', justifyContent: 'center' }}>
        <ReactCrop
          crop={crop}
          src={rootImg}
          onImageLoaded={onLoad}
          onComplete={(c) => {
            setCompletedCrop(c);
          }}
          onChange={(c) => {
            setCrop(c);
          }}
        />
      </div>

      <div style={{ display: 'none' }}>
        <canvas
          ref={previewCanvasRef}
          // Rounding is important so the canvas width and height matches/is a multiple for sharpness.
          // style={{
          //   width: Math.round(completedCrop?.width ?? 0),
          //   height: Math.round(completedCrop?.height ?? 0),
          // }}
        />
      </div>
    </ModalCommon>
  );
};

const mapStateToProps = (state) => ({
  imageHeader: state?.entities?.signature?.__ELEMENTS?.find((e) => e?.type === BLOCKTYPE_SIGNATURE.HEADER)?.imageMainCoverSize,
});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(ModalCropPhoto);
