import createReducer, { createConsumeEntities } from 'store/createReducer';
import AuthActionTypes from 'components/Auth/auth.actions';
import { BLOCKTYPE_SIGNATURE, RIGHT_MENU_TYPE_SIGNATURE } from '../../Constants';
import { SignatureActions } from './signature.actions';

const TEMPLATE_DATA_BLOCK = {
  HEADER: {
    layout: 'layout1',
    background: null,
    backgroundFilter: null,
    backgroundColor: null,
    textColor: null,
    textAlign: 'left',
    // content: '<h1><strong>PROPOSAL</strong></h1>',
    content: '',
    contentPdf: null,
  },
  PARTER: {
    __COMPANY_CONTACT_INFO: [
      {
        companyName: { show: true, value: '' },
        vatNumber: { show: true, value: '' },
        address: { show: true, value: '' },
        fullName: { show: true, value: '' },
        email: { show: true, value: '' },
        phone: { show: true, value: '' },
        title: { show: true, value: '' },
      },
    ],
    __USER_INFO: [
      {
        companyName: { show: true, value: '' },
        vatNumber: { show: true, value: '' },
        address: { show: true, value: '' },
        fullName: { show: true, value: '' },
        email: { show: true, value: '' },
        phone: { show: true, value: '' },
        title: { show: true, value: '' },
      },
    ],
  },
  TEXT: {
    content: '',
  },
  COVER: {
    backgroundType: 'color',
    backgroundColor: '#b1dce2',
    content: '',
    // content: '<h1 class="ql-align-center"><span style="color: rgb(255, 255, 255);">An interesting title</span></h1>',
  },
  ATTACHMENT: {
    attachments: [],
    attachmentsUrl: [],
  },
  IMAGE: {
    img: '',
    width: '100%',
    height: 'auto',
    align: 'left',
    imgPercent: ''
  },
  PRICING: {
    vat: 0,
  },
  COLUMN: {
    __ELEMENTS: [[], []],
  },
  SPACE: {
    height: 100
  }
};
export const initialState = {
  objectType: null,
  isShowPDF: false,
  __MODE: 'BUILD', //BUILD or PREVIEW
  // __ELEMENTS: [
  //   {
  //     type: BLOCKTYPE_SIGNATURE.HEADER,
  //     layout: 'layout1',
  //     background: null,
  //     backgroundFilter: null,
  //     backgroundColor: null,
  //     textColor: null,
  //     textAlign: 'left',
  //     content: '<h1><strong>PROPOSAL</strong></h1>',
  //   },
  //   {
  //     type: BLOCKTYPE_SIGNATURE.PARTER,
  //     __COMPANY_CONTACT_INFO: {
  //       companyName: { show: true, value: '' },
  //       vatNumber: { show: true, value: '' },
  //       address: { show: true, value: '' },
  //       fullName: { show: true, value: '' },
  //       email: { show: true, value: '' },
  //       phone: { show: true, value: '' },
  //       title: { show: true, value: '' },
  //     },
  //     __USER_INFO: {
  //       companyName: { show: true, value: '' },
  //       vatNumber: { show: true, value: '' },
  //       address: { show: true, value: '' },
  //       fullName: { show: true, value: '' },
  //       email: { show: true, value: '' },
  //       phone: { show: true, value: '' },
  //       title: { show: true, value: '' },
  //     },
  //   },
  //   {
  //     type: BLOCKTYPE_SIGNATURE.PRICING,
  //   },
  //   {
  //     type: BLOCKTYPE_SIGNATURE.TEXT,
  //     content: '<h1><strong>Title</strong></h1>',
  //   },
  //   {
  //     type: BLOCKTYPE_SIGNATURE.ACCEPT,
  //   },
  // ],
  __ELEMENTS: [],
  RIGHT_MENU_TYPE: null,
  showRightMenu: false,
  blockIndexShowRightmenu: null,
  __DEAL: {},
  __COMPANY: {},
  __CONTACT: {},
  __USER_LOGIN_INFO: {},
  __PRODUCT: [],
  __TOTAL_VALUE_PRODUCT: {
    total: 0,
    vat: 0,
    net: 0,
  },
  language: null,
  currency: null,
  __SETTING_COLUMN_PRODUCT: {
    productGroup: false,
    product: true,
    productType: false,
    startDate: false,
    endDate: false,
    unitAmount: true,
    unitPrice: true,
    unitCost: false,
    discountPercent: false,
    discountPrice: false,
    discountAmount: false,
    margin: false,
    cost: false,
    value: true,
    profit: false,
    occupied: false,
    description: false,
  },
  positionBlockLastestFocused: 0,
  valueTobeFilledToEditor: null,
  isShowingModalCropPhoto: false,
};
const consumeEntities = createConsumeEntities('signature');

const calculateProduct = (products, vat) => {
  let net = 0;
  products?.map((e) => {
    let cost = (e.costUnit || 0) * (e.numberOfUnit || 0);
    let value =
      e.type === 'RECURRING' && e.periodNumber ? e.numberOfUnit * e.discountedPrice * e.periodNumber : e.numberOfUnit * e.discountedPrice;
    let profit = e.numberOfUnit * e.price - (e.costUnit || 0) * (e.numberOfUnit || 0);

    net += value;
  });

  let total = net + (net * vat) / 100;
  return {
    net: net,
    total: total,
  };
};
export default createReducer(initialState, {
  default: consumeEntities,
  [AuthActionTypes.LOGOUT]: (draft) => {
    Object.keys(draft).forEach((id) => {
      delete draft[id];
    });
  },
  [SignatureActions.RESET_DEFAULT_DATA]: (draft, {}) => {
    Object.keys(draft).forEach((id) => {
      delete draft[id];
    });
    draft = initialState;
  },
  [SignatureActions.SET_OBJECT_TYPE_TEMPLATE]: (draft, { objectType }) => {
    draft.objectType = objectType;
  },
  [SignatureActions.ADD_BLOCK]: (draft, { blockType, index, dataTrans }) => {
    if (!draft.__SETTING_COLUMN_PRODUCT) {
      draft.__SETTING_COLUMN_PRODUCT = {
        productGroup: false,
        product: true,
        productType: false,
        unitAmount: true,
        unitPrice: true,
        unitCost: false,
        discountPercent: false,
        discountPrice: false,
        discountAmount: false,
        margin: false,
        cost: false,
        value: true,
        profit: false,
      };
    }
    if (!draft.__ELEMENTS) {
      draft.__ELEMENTS = [];
    }
    if (blockType === BLOCKTYPE_SIGNATURE.PARTER) {
      if ((!index && index !== 0) || index > draft.__ELEMENTS.length) {
        draft.__ELEMENTS.push({
          type: blockType,
          __COMPANY_CONTACT_INFO: [
            {
              companyName: { show: true, value: draft.__COMPANY?.name || '' },
              vatNumber: { show: true, value: draft.__COMPANY?.vatNumber || '' },
              address: { show: true, value: draft.__COMPANY?.fullAddress || '' },
              fullName: {
                show: true,
                value: `${draft.__CONTACT?.firstName || ''} ${draft.__CONTACT?.lastName || ''}`,
              },
              email: { show: true, value: draft.__CONTACT?.email || '' },
              phone: { show: true, value: draft.__CONTACT?.phone || '' },
              title: { show: true, value: draft.__CONTACT?.title || '' },
              firstName: { show: true, value: draft.__CONTACT?.firstName || '' },
              lastName: { show: true, value: draft.__CONTACT?.lastName || '' },
              country: { show: true, value: draft.__CONTACT?.country || null },
            },
          ],
          __USER_INFO: [
            {
              companyName: { show: true, value: draft?.__USER_LOGIN_INFO?.companyName || '' },
              vatNumber: { show: true, value: draft?.__USER_LOGIN_INFO?.vatNumber || '' },
              address: { show: true, value: draft?.__USER_LOGIN_INFO?.address || '' },
              fullName: { show: true, value: draft?.__USER_LOGIN_INFO?.fullName || '' },
              email: { show: true, value: draft?.__USER_LOGIN_INFO?.email || '' },
              phone: { show: true, value: draft?.__USER_LOGIN_INFO?.phone || '' },
              title: { show: true, value: '' },
              firstName: { show: true, value: draft?.__USER_LOGIN_INFO?.firstName },
              lastName: { show: true, value: draft?.__USER_LOGIN_INFO?.lastName },
              country: { show: true, value: draft.__USER_LOGIN_INFO?.country || null },
            },
          ],
        });
      } else {
        draft.__ELEMENTS = [
          ...draft.__ELEMENTS.slice(0, index),
          {
            type: blockType,
            __COMPANY_CONTACT_INFO: [
              {
                companyName: { show: true, value: draft.__COMPANY?.name || '' },
                vatNumber: { show: true, value: draft.__COMPANY?.vatNumber || '' },
                address: { show: true, value: draft.__COMPANY?.fullAddress || '' },
                fullName: {
                  show: true,
                  value: `${draft.__CONTACT?.firstName || ''} ${draft.__CONTACT?.lastName || ''}`,
                },
                email: { show: true, value: draft.__CONTACT?.email || '' },
                phone: { show: true, value: draft.__CONTACT?.phone || '' },
                title: { show: true, value: draft.__CONTACT?.title || '' },
                firstName: { show: true, value: draft.__CONTACT?.firstName || '' },
                lastName: { show: true, value: draft.__CONTACT?.lastName || '' },
                country: { show: true, value: draft.__CONTACT?.country || null },
              },
            ],
            __USER_INFO: [
              {
                companyName: { show: true, value: draft?.__USER_LOGIN_INFO?.companyName || '' },
                vatNumber: { show: true, value: draft?.__USER_LOGIN_INFO?.vatNumber || '' },
                address: { show: true, value: draft?.__USER_LOGIN_INFO?.address || '' },
                fullName: { show: true, value: draft?.__USER_LOGIN_INFO?.fullName || '' },
                email: { show: true, value: draft?.__USER_LOGIN_INFO?.email || '' },
                phone: { show: true, value: draft?.__USER_LOGIN_INFO?.phone || '' },
                title: { show: true, value: '' },
                firstName: { show: true, value: draft?.__USER_LOGIN_INFO?.firstName },
                lastName: { show: true, value: draft?.__USER_LOGIN_INFO?.lastName },
                country: { show: true, value: draft.__USER_LOGIN_INFO?.country || null },
              },
            ],
          },
          ...draft.__ELEMENTS.slice(index, draft.__ELEMENTS.length),
        ];
      }
      return;
    }
    if (blockType === BLOCKTYPE_SIGNATURE.TEXT) {
      if ((!index && index !== 0) || index > draft.__ELEMENTS.length) {
        draft.__ELEMENTS.push({
          type: blockType,
          content: '<h1><strong>' + dataTrans + '</strong></h1>',
          contentTemp: '<h1><strong>' + dataTrans + '</strong></h1>',
        });
      } else {
        draft.__ELEMENTS = [
          ...draft.__ELEMENTS.slice(0, index),
          {
            type: blockType,
            content: '<h1><strong>' + dataTrans + '</strong></h1>',
            contentTemp: '<h1><strong>' + dataTrans + '</strong></h1>',
          },
          ...draft.__ELEMENTS.slice(index, draft.__ELEMENTS.length),
        ];
      }
      return;
    }
    if (blockType === BLOCKTYPE_SIGNATURE.COVER) {
      if ((!index && index !== 0) || index > draft.__ELEMENTS.length) {
        draft.__ELEMENTS.push({
          type: blockType,
          ...TEMPLATE_DATA_BLOCK[blockType],
          content: '<h1 class="ql-align-center"><span style="color: rgb(255, 255, 255);">' + dataTrans + '</span></h1>',
          contentTemp:
            '<h1 class="ql-align-center"><span style="color: rgb(255, 255, 255);">' + dataTrans + '</span></h1>',
        });
      } else {
        draft.__ELEMENTS = [
          ...draft.__ELEMENTS.slice(0, index),
          {
            type: blockType,
            ...TEMPLATE_DATA_BLOCK[blockType],
            content:
              '<h1 class="ql-align-center"><span style="color: rgb(255, 255, 255);">' + dataTrans + '</span></h1>',
            contentTemp:
              '<h1 class="ql-align-center"><span style="color: rgb(255, 255, 255);">' + dataTrans + '</span></h1>',
          },
          ...draft.__ELEMENTS.slice(index, draft.__ELEMENTS.length),
        ];
      }
      return;
    }
    if (blockType === BLOCKTYPE_SIGNATURE.HEADER) {
      if ((!index && index !== 0) || index > draft.__ELEMENTS.length) {
        draft.__ELEMENTS.push({
          type: blockType,
          ...TEMPLATE_DATA_BLOCK[blockType],
          content: '<h1><strong>'+dataTrans.toUpperCase()+'</strong></h1>',
          contentTemp: '<h1><strong>'+dataTrans.toUpperCase()+'</strong></h1>'
        });
      } else {
        draft.__ELEMENTS = [
          ...draft.__ELEMENTS.slice(0, index),
          {
            type: blockType,
            ...TEMPLATE_DATA_BLOCK[blockType],
            content: '<h1><strong>'+dataTrans.toUpperCase()+'</strong></h1>',
            contentTemp: '<h1><strong>'+dataTrans.toUpperCase()+'</strong></h1>'
          },
          ...draft.__ELEMENTS.slice(index, draft.__ELEMENTS.length),
        ];
      }
      return;
    }
    if ((!index && index !== 0) || index > draft.__ELEMENTS.length) {
      draft.__ELEMENTS.push({ type: blockType, ...TEMPLATE_DATA_BLOCK[blockType] });
    } else {
      draft.__ELEMENTS = [
        ...draft.__ELEMENTS.slice(0, index),
        { type: blockType, ...TEMPLATE_DATA_BLOCK[blockType] },
        ...draft.__ELEMENTS.slice(index, draft.__ELEMENTS.length),
      ];
    }
  },
  [SignatureActions.REMOVE_BLOCK]: (draft, { index }) => {
    draft.__ELEMENTS = [
      ...draft.__ELEMENTS.slice(0, index),
      ...draft.__ELEMENTS.slice(index + 1, draft.__ELEMENTS.length),
    ];
  },
  [SignatureActions.REORDER_ELEMENT]: (draft, { oldIndex, newIndex }) => {
    const [reorderedItem] = draft.__ELEMENTS.splice(oldIndex, 1);
    draft.__ELEMENTS.splice(newIndex, 0, reorderedItem);
  },
  [SignatureActions.SETUP_DATA_FROM_DEAL]: (draft, { data }) => {
    draft.__DEAL = { ...data };
  },
  [SignatureActions.SETUP_DATA_FROM_COMPANY]: (draft, { data }) => {
    draft.__COMPANY = { ...data };
  },
  [SignatureActions.SETUP_DATA_FROM_CONTACT]: (draft, { data }) => {
    draft.__CONTACT = { ...data };
  },
  [SignatureActions.SET_VISIBLE_RIGHT_MENU]: (draft, { visible, blockType, blockIndex }) => {
    draft.showRightMenu = visible;
    draft.RIGHT_MENU_TYPE = blockType;
    draft.blockIndexShowRightmenu = blockIndex;
  },
  [SignatureActions.SELECT_MODE]: (draft, { mode }) => {
    draft.__MODE = mode;
  },
  [SignatureActions.EDIT_HEADER]: (draft, { key, value, valuePdf }) => {
    draft.__ELEMENTS.forEach((e) => {
      if (e.type === BLOCKTYPE_SIGNATURE.HEADER) {
        e[key] = value;
        if (valuePdf) e['contentPdf'] = valuePdf;
      }
    });
  },
  [SignatureActions.EDIT_COVER]: (draft, { key, value, valuePdf, blockIndex }) => {
    // let index = blockIndex || draft.blockIndexShowRightmenu;

    if (draft.__ELEMENTS[blockIndex]) {
      draft.__ELEMENTS[blockIndex][key] = value;
      if(valuePdf){
        draft.__ELEMENTS[blockIndex]['valuePdf'] = valuePdf;
      }
    }
  },
  [SignatureActions.EDIT_TEXT_BLOCK]: (draft, { key, value, blockIndex, valuePdf }) => {
    draft.__ELEMENTS[blockIndex][key] = value;
    if (valuePdf) {
      draft.__ELEMENTS[blockIndex]['valuePdf'] = valuePdf;
    }
  },
  [SignatureActions.EDIT_TEXT_BLOCK_IN_COLUMN_BLOCK]: (
    draft,
    { parentBlockIndex, columnIndex, key, value, blockIndex, valuePdf }
  ) => {
    if(parentBlockIndex !== undefined && columnIndex !== undefined) {
      draft.__ELEMENTS[parentBlockIndex].__ELEMENTS[columnIndex][blockIndex][key] = value;
      if (valuePdf) {
        draft.__ELEMENTS[parentBlockIndex].__ELEMENTS[columnIndex][blockIndex]['valuePdf'] = valuePdf;
      }
    }

  },
  [SignatureActions.ADD_IMAGE_BLOCK]: (draft, { index, imgBase64, widths }) => {
    draft.__ELEMENTS[index] = {
      ...draft.__ELEMENTS[index],
      img: imgBase64,
      width: widths,
    };
  },
  [SignatureActions.ADD_IMAGE_BLOCK_IN_COLUMN_BLOCK]: (
    draft,
    { blockIndex, columnIndex, index, imgBase64, widths }
  ) => {
    draft.__ELEMENTS[blockIndex].__ELEMENTS[columnIndex][index] = {
      ...draft.__ELEMENTS[blockIndex].__ELEMENTS[columnIndex][index],
      img: imgBase64,
      width: widths,
    };
  },
  [SignatureActions.EDIT_IMAGE_BLOCK]: (draft, { index, key, value }) => {
    draft.__ELEMENTS[index][key] = value;
  },
  [SignatureActions.SET_PRODUCT_DATA]: (draft, { data }) => {
    draft.__PRODUCT = data;
    if (!draft.__TOTAL_VALUE_PRODUCT) {
      draft.__TOTAL_VALUE_PRODUCT = {
        total: 0,
        net: 0,
        vat: 0,
      };
    }
    let calculateValue = calculateProduct(draft.__PRODUCT, 0) || {};
    draft.__TOTAL_VALUE_PRODUCT.vat = 0;
    draft.__TOTAL_VALUE_PRODUCT.total = calculateValue.total || 0;
    draft.__TOTAL_VALUE_PRODUCT.net = calculateValue.net || 0;
  },
  [SignatureActions.EDIT_PARTER_BLOCK]: (draft, { parent, data, index }) => {
    draft.__ELEMENTS.forEach((e) => {
      if (e.type === BLOCKTYPE_SIGNATURE.PARTER) {
        e[parent][index] = {
          companyName: { show: e[parent][index].companyName.show, value: data.name.value },
          vatNumber: { show: e[parent][index].vatNumber.show, value: data.vat.value },
          address: { show: e[parent][index].address.show, value: data.address.value },
          fullName: { show: e[parent][index].fullName.show, value: data.fullName.value },
          email: { show: e[parent][index].email.show, value: data.email.value },
          phone: { show: e[parent][index].phone.show, value: data.phone.value },
          title: { show: e[parent][index].title.show, value: data.title.value },
          firstName: { show: e[parent][index].firstName.show, value: data.firstName.value },
          lastName: { show: e[parent][index].lastName.show, value: data.lastName.value },
          country: { show: e[parent][index].country.show, value: data.country.value },
        };
      }
    });
  },
  [SignatureActions.SETUP_DATA_PARTER_BLOCK]: (draft, { data }) => {
    draft.__ELEMENTS?.forEach((e) => {
      if (e.type === BLOCKTYPE_SIGNATURE.PARTER) {
        e = { ...e, ...data };
      }
    });
  },
  [SignatureActions.TOGGLE_COLUMN_PRODUCT_BLOCK]: (draft, { key }) => {
    if (!draft.__SETTING_COLUMN_PRODUCT) {
      draft.__SETTING_COLUMN_PRODUCT = {};
    }
    draft.__SETTING_COLUMN_PRODUCT[key] = !draft.__SETTING_COLUMN_PRODUCT?.[key];
  },
  [SignatureActions.SET_DATA_FILL_TO_EDITOR]: (draft, { data }) => {
    draft.valueTobeFilledToEditor = data;
  },
  [SignatureActions.SET_EDIT_CONTENT]: (draft, { data, index, pdfToSave }) => {
    draft.__ELEMENTS[index]['contentTemp'] = data;
    draft.__ELEMENTS[index]['pdfToSave'] = pdfToSave;
  },
  [SignatureActions.SET_EDIT_CONTENT_IN_COLUMN_BLOCK]: (draft, { blockIndex, columnIndex, data, index, pdfToSave }) => {
    draft.__ELEMENTS[blockIndex].__ELEMENTS[columnIndex][index]['contentTemp'] = data;
    draft.__ELEMENTS[blockIndex].__ELEMENTS[columnIndex][index]['pdfToSave'] = pdfToSave;
  },
  [SignatureActions.CONVERT_OPS_CONTENT]: (draft, {index, key, value}) => {
    draft.__ELEMENTS[index][key] = value
  },
  [SignatureActions.SET_POSITION_BLOCK_LASTEST_FOCUSED]: (draft, { index, parentBlockIndex, parentColumnIndex }) => {
    draft.positionBlockLastestFocused = index;
    draft.positionParentBlockLastestFocused = parentBlockIndex;
    draft.positionColumnLastestFocused = parentColumnIndex;
  },
  [SignatureActions.IS_UPLOAD_PDF]: (draft, { index }) => {
    draft.isShowPDF = index;
  },
  [SignatureActions.SETUP_DATA_FROM_USER_LOGIN]: (draft, { data }) => {
    draft.__USER_LOGIN_INFO = { ...data };
  },
  [SignatureActions.UPLOAD_ATTACHMENT]: (draft, { files }) => {
    draft.__ELEMENTS?.forEach((e) => {
      if (e.type === BLOCKTYPE_SIGNATURE.ATTACHMENT) {
        e.attachments = files;
      }
    });
  },
  [SignatureActions.UPLOAD_URL_ATTACHMENTS]: (draft, { url }) => {
    draft.__ELEMENTS?.forEach((e) => {
      if (e.type === BLOCKTYPE_SIGNATURE.ATTACHMENT) {
        e.attachmentsUrl = url;
      }
    });
  },
  [SignatureActions.INIT_DATA_TEMPLATE_SELECTED]: (draft, { data }) => {
    draft.__ELEMENTS = data.__ELEMENTS;
    draft.__SETTING_COLUMN_PRODUCT = data.__SETTING_COLUMN_PRODUCT;
  },
  [SignatureActions.CHANGE_VAT_VALUE]: (draft, { value }) => {
    if (!draft.__TOTAL_VALUE_PRODUCT) {
      draft.__TOTAL_VALUE_PRODUCT = {
        total: 0,
        net: 0,
        vat: 0,
      };
    }
    let i = draft.__ELEMENTS.indexOf(draft.__ELEMENTS.find((e) => e.type === 'PRICING'));
    draft.__ELEMENTS[i]['vat'] = parseInt(value);
    let calculateValue = calculateProduct(draft.__PRODUCT, value) || {};
    draft.__TOTAL_VALUE_PRODUCT.vat = value;
    draft.__TOTAL_VALUE_PRODUCT.total = calculateValue.total || 0;
    draft.__TOTAL_VALUE_PRODUCT.net = calculateValue.net || 0;
  },
  [SignatureActions.CHANG_VALUE_SHOW_CROP_PHOTO_MODAL]: (draft, { value }) => {
    draft.isShowingModalCropPhoto = value;
  },
  [SignatureActions.SET_REVENUETYPE]: (draft, { revenueType }) => {
    draft.__REVENUETYPE = revenueType;
  },
  [SignatureActions.ADD_MORE_PARTER]: (draft, { parent, data }) => {
    draft.__ELEMENTS.forEach((e) => {
      if (e.type === BLOCKTYPE_SIGNATURE.PARTER) {
        e[parent].push(data);
      }
    });
  },
  [SignatureActions.SET_SIGNATURE_LANGUAGE]: (draft, { value }) => {
    draft.language = value;
  },
  [SignatureActions.SET_SIGNATURE_CURRENCY]: (draft, { value }) => {
    draft.currency = value;
  },
  [SignatureActions.DELETE_ONE_PARTY]: (draft, { typeD, index }) => {
    draft.__ELEMENTS.forEach((e) => {
      if (e.type === BLOCKTYPE_SIGNATURE.PARTER) {
        e[typeD].splice(index);
        if (e[typeD].length === 0 && typeD !== '__COMPANY_CONTACT_INFO') delete e[typeD];
      }
    });
  },
  [SignatureActions.CHANGE_URL_TERM]: (draft, { url }) => {
    draft.__ELEMENTS.forEach((e) => {
      if (e.type === BLOCKTYPE_SIGNATURE.TERM) {
        e['urlTerm'] = url;
      }
    });
  },
  [SignatureActions.SELECT_TOTAL_COLUMN]: (draft, { blockIndex, total }) => {
    if (!draft.__ELEMENTS[blockIndex].__ELEMENTS) {
      draft.__ELEMENTS[blockIndex].__ELEMENTS = [];
    }
    let newArr = [];
    for (let i = 0; i < total; i++) {
      newArr.push(draft.__ELEMENTS[blockIndex].__ELEMENTS[i] || []);
    }
    draft.__ELEMENTS[blockIndex].__ELEMENTS = newArr;
  },
  [SignatureActions.REORDER_ELEMENT_IN_COLUMN_BLOCK]: (
    draft,
    { blockIndex, sourceColumnIndex, sourceElementIndex, destinationColumnIndex, destinationElementIndex }
  ) => {
    if (sourceColumnIndex === destinationColumnIndex) {
      // reorder element in one column
      const [reorderedItem] = draft.__ELEMENTS[blockIndex].__ELEMENTS[sourceColumnIndex].splice(sourceElementIndex, 1);
      draft.__ELEMENTS[blockIndex].__ELEMENTS[sourceColumnIndex].splice(destinationElementIndex, 0, reorderedItem);
    } else {
      // move element between other column
      const sourceColumnClone = Array.from(draft.__ELEMENTS[blockIndex].__ELEMENTS[sourceColumnIndex]);
      const destColumnClone = Array.from(draft.__ELEMENTS[blockIndex].__ELEMENTS[destinationColumnIndex]);
      const [removed] = sourceColumnClone.splice(sourceElementIndex, 1);

      destColumnClone.splice(destinationElementIndex, 0, removed);
      draft.__ELEMENTS[blockIndex].__ELEMENTS[sourceColumnIndex] = sourceColumnClone;
      draft.__ELEMENTS[blockIndex].__ELEMENTS[destinationColumnIndex] = destColumnClone;
    }
  },
  [SignatureActions.ADD_ELEMENT_TO_COLUMN_BLOCK]: (
    draft,
    { blockIndex, destinationColumnIndex, destinationElementIndex, blockType, dataTrans }
  ) => {
    const destColumnClone = Array.from(draft.__ELEMENTS[blockIndex].__ELEMENTS[destinationColumnIndex]);
    destColumnClone.splice(destinationElementIndex, 0, { type: blockType, content: blockType === BLOCKTYPE_SIGNATURE.TEXT ? '<h1><strong>' + dataTrans + '</strong></h1>' : null});
    draft.__ELEMENTS[blockIndex].__ELEMENTS[destinationColumnIndex] = destColumnClone;
  },
  [SignatureActions.REMOVE_ELEMENT_IN_COLUMN_BLOCK]: (draft, { blockIndex, columnIndex, elementIndex }) => {
    const destColumnClone = Array.from(draft.__ELEMENTS[blockIndex].__ELEMENTS[columnIndex]);
    destColumnClone.splice(elementIndex, 1);
    draft.__ELEMENTS[blockIndex].__ELEMENTS[columnIndex] = destColumnClone;
  },
  [SignatureActions.EDIT_HEIGHT_SPACE_BLOCK]: (draft, {blockIndex, height}) => {
    draft.__ELEMENTS[blockIndex].height = height;
  }
});
