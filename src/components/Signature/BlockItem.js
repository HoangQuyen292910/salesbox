import React from 'react';
import style from './Signature.css';
import cx from 'classnames';
import _l from 'lib/i18n';

function BlockItem(props) {
  const { item, isDragging, disabled } = props;
  return (
    <div className={cx(style.blockItem, isDragging && style.blockIsDragging, disabled && style.disableDragging)}>
      {item.icon && (
        <div className={cx(style[item.icon], style.iconBlockItem)}>
          <img src={item.icon} />
        </div>
      )}
      {!item.uuid
        ? item.title.includes('Company VAT')
          ? _l.call(this, [item.title])
          : item.title.includes('Company')
          ? _l.call(this, [item.title.replace('Company ', '')])
          : item.title.includes('Contact')
          ? _l.call(this, [item.title.replace('Contact ', '')])
          : _l.call(this, [item.title])
        : item.title}
    </div>
  );
}

export default BlockItem;
