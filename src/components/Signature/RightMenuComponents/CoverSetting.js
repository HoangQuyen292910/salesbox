import React, { useState } from 'react';
import { connect } from 'react-redux';
import _l from 'lib/i18n';
import { Button, Divider, Input } from 'semantic-ui-react';
import style from '../Signature.css';
import cx from 'classnames';
import { editCover, changeValueShowCropPhotoModal } from '../signature.actions';
import ModalCropPhoto from '../ModalCropPhoto';
export const Cover = (props) => {
  const { blockIndex, editCover, __ELEMENTS_DATA, changeValueShowCropPhotoModal } = props;

  const styleInputColor = { width: 30, height: 30, padding: 0 };
  const selectBackground = (e) => {
    if (e.target.files && e.target.files.length > 0) {
      const reader = new FileReader();
      // reader.addEventListener('load', () => setImgBg(reader.result));
      reader.addEventListener('load', () => {
        setSelectedImage(reader.result);
        setVisibleCropPhotoModal(true);
        changeValueShowCropPhotoModal(true);
      });
      reader.readAsDataURL(e.target.files[0]);
    }
  };
  const [visibleCropPhotoModal, setVisibleCropPhotoModal] = useState(false);
  const [selectedImage, setSelectedImage] = useState(null);

  return (
    <div style={{ paddingTop: 20 }}>
      <h4>{_l`Background`}</h4>
      <div className={style.listCoverSetting}>
        <Button
          className={cx(
            style.btnSettingCover,
            __ELEMENTS_DATA?.backgroundType === 'color' && style.itemMenuLayoutActive
          )}
          onClick={() => editCover('backgroundType', 'color', blockIndex, blockIndex)}
        >{_l`Color`}</Button>
        <Button
          className={cx(
            style.btnSettingCover,
            __ELEMENTS_DATA?.backgroundType === 'gradient' && style.itemMenuLayoutActive
          )}
          onClick={() => editCover('backgroundType', 'gradient', blockIndex, blockIndex)}
        >{_l`Gradient`}</Button>
        <Button
          className={cx(
            style.btnSettingCover,
            __ELEMENTS_DATA?.backgroundType === 'image' && style.itemMenuLayoutActive
          )}
          onClick={() => {
            editCover('backgroundType', 'image', blockIndex, blockIndex);
            editCover('backgroundImage', 'https://picsum.photos/500/200', blockIndex, blockIndex);
          }}
        >{_l`Image`}</Button>
      </div>
      <br />
      {__ELEMENTS_DATA?.backgroundType === 'color' && (
        <div className={style.dflex}>
          <h4>{_l`Color`}</h4>
          <input
            type="color"
            style={styleInputColor}
            value={__ELEMENTS_DATA?.backgroundColor}
            onChange={(e) => editCover('backgroundColor', e.target.value, blockIndex, blockIndex)}
          />
        </div>
      )}
      {__ELEMENTS_DATA?.backgroundType === 'gradient' && (
        <>
          <div className={style.dflex}>
            <h4>{_l`Start color`}</h4>
            <input
              type="color"
              style={styleInputColor}
              value={__ELEMENTS_DATA?.backgroundGradientStartColor}
              onChange={(e) => editCover('backgroundGradientStartColor', e.target.value, blockIndex, blockIndex)}
            />
          </div>
          <Divider />
          <div className={style.dflex}>
            <h4>{_l`End color`}</h4>
            <input
              type="color"
              style={styleInputColor}
              value={__ELEMENTS_DATA?.backgroundGradientEndColor}
              onChange={(e) => editCover('backgroundGradientEndColor', e.target.value, blockIndex, blockIndex)}
            />
          </div>
          {/* <Divider />
          <div className={style.dflex}>
            <h4>{_l`Rotation`}</h4>
            <input
              type="number"
              value={__ELEMENTS_DATA?.rotation}
              onChange={(e) => editCover('rotation', e.target.value, blockIndex)}
            />
          </div> */}
        </>
      )}
      {__ELEMENTS_DATA?.backgroundType === 'image' && (
        <>
          <img
            alt=""
            src={__ELEMENTS_DATA?.backgroundImage || 'https://picsum.photos/200/300'}
            height={100}
            style={{ width: '100%' }}
            onClick={() => {
              document.getElementById('fileBackgroundHeader').click();
            }}
          />
          <input accept="image/*" type="file" hidden id="fileBackgroundHeader" onChange={selectBackground} />
        </>
      )}

      <ModalCropPhoto
        visible={visibleCropPhotoModal}
        onClose={() => {
          setVisibleCropPhotoModal(false);
          changeValueShowCropPhotoModal(false);
        }}
        onDone={(result) => {
          editCover('backgroundImage', result, blockIndex, blockIndex);
          setVisibleCropPhotoModal(false);
          changeValueShowCropPhotoModal(false);
        }}
        rootImg={selectedImage}
      />
    </div>
  );
};

const mapStateToProps = (state) => {
  const blockIndex = state.entities?.signature?.blockIndexShowRightmenu;
  return {
    blockIndex,
    __ELEMENTS_DATA: state.entities?.signature?.__ELEMENTS?.[blockIndex],
  };
};

const mapDispatchToProps = {
  editCover,
  changeValueShowCropPhotoModal,
};

export default connect(mapStateToProps, mapDispatchToProps)(Cover);
