import React from 'react';
import { connect } from 'react-redux';
import { RIGHT_MENU_TYPE_SIGNATURE } from '../../Constants';
import HeaderSetting from './RightMenuComponents/HeaderSetting';
import CoverSetting from './RightMenuComponents/CoverSetting';
import VideoSetting from './RightMenuComponents/VideoSetting';
import GeneralSetting from './RightMenuComponents/GeneralSetting';
import ImageSetting from './RightMenuComponents/ImageSetting';
import ColumnSetting from './RightMenuComponents/ColumnSetting';
import SpaceSetting from './RightMenuComponents/SpaceSetting'
export const RightMenu = (props) => {
  const { type } = props;

  if (type === RIGHT_MENU_TYPE_SIGNATURE.HEADER) {
    return <HeaderSetting />;
  }
  if (type === RIGHT_MENU_TYPE_SIGNATURE.COVER) {
    return <CoverSetting />;
  }
  if (type === RIGHT_MENU_TYPE_SIGNATURE.VIDEO) {
    return <VideoSetting />;
  }
  if (type === RIGHT_MENU_TYPE_SIGNATURE.SETTING) {
    return <GeneralSetting />;
  }
  if(type === RIGHT_MENU_TYPE_SIGNATURE.IMAGE){
    return <ImageSetting/>;
  }
  if(type === RIGHT_MENU_TYPE_SIGNATURE.COLUMN){
    return <ColumnSetting/>;
  }
  if(type === RIGHT_MENU_TYPE_SIGNATURE.SPACE){
    return <SpaceSetting/>;
  }
  return <div></div>;
};

const mapStateToProps = (state) => ({
  type: state.entities?.signature?.RIGHT_MENU_TYPE,
});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(RightMenu);
