export const SignatureActions = {
  SET_VISIBLE_RIGHT_MENU: 'signature/setVisibleRightMenu',
  REORDER_ELEMENT: 'signature/reorderElement',
  ADD_BLOCK: 'signature/addBlock',
  REMOVE_BLOCK: 'signature/removeBlock',
  EDIT_BLOCK: 'signature/editBlock',
  SETUP_DATA_FROM_DEAL: 'signature/setupDataFromDeal',
  SETUP_DATA_FROM_COMPANY: 'signature/setupDataFromCompany',
  SETUP_DATA_FROM_CONTACT: 'signature/setupDataFromContact',
  SELECT_MODE: 'signature/selectMode',
  EDIT_HEADER: 'signature/editHeader',
  EDIT_COVER: 'signature/editCover',
  EDIT_TEXT_BLOCK: 'signature/editTextBlock',
  SET_PRODUCT_DATA: 'signature/setProductData',
  SETUP_DATA_PARTER_BLOCK: 'signature/setupDataParterBlock',
  EDIT_PARTER_BLOCK: 'signature/editParterBlock',
  TOGGLE_COLUMN_PRODUCT_BLOCK: 'signature/toggleColumnProductBlock',
  SET_DATA_FILL_TO_EDITOR: 'signature/setDataFillToEditor',
  SET_EDIT_CONTENT: 'signature/setEditContent',
  SET_POSITION_BLOCK_LASTEST_FOCUSED: 'signature/setPositionBlockLastestFocused',
  IS_UPLOAD_PDF: 'signature/isUploadPDF',
  RESET_DEFAULT_DATA: 'signature/resetDefaultData',
  SETUP_DATA_FROM_USER_LOGIN: 'signature/setupDataFromUserLogin',
  SET_OBJECT_TYPE_TEMPLATE: 'signature/setObjectTypeTemplate',
  UPLOAD_ATTACHMENT: 'signature/uploadAttachment',
  UPLOAD_URL_ATTACHMENTS: 'signature/uploadUrlAttachment',
  INIT_DATA_TEMPLATE_SELECTED: 'signature/initDataTemplateSelected',
  CHANGE_VAT_VALUE: 'signature/changeVatValue',
  CHANG_VALUE_SHOW_CROP_PHOTO_MODAL: 'signature/changeValueShowCropPhotoModal',
  SET_REVENUETYPE: 'signature/setRevenueType',
  ADD_MORE_PARTER: 'signature/addMoreParter',
  SET_SIGNATURE_LANGUAGE: 'signature/setSignatureLanguage',
  SET_SIGNATURE_CURRENCY: 'signature/setSignatureCurrency',
  DELETE_ONE_PARTY: 'signature/deleteOneParty',
  CHANGE_URL_TERM: 'signature/changeUrlTerm',
  ADD_IMAGE_BLOCK: 'signature/addImageBlock',
  EDIT_IMAGE_BLOCK: 'signature/editImageBlock',
  SELECT_TOTAL_COLUMN: 'signature/selectTotalColumn',
  REORDER_ELEMENT_IN_COLUMN_BLOCK: 'signature/reorderElementInColumnBlock',
  ADD_ELEMENT_TO_COLUMN_BLOCK: 'signature/addElementToColumnBlock',
  REMOVE_ELEMENT_IN_COLUMN_BLOCK: 'signature/removeElementInColumnBlock',
  SET_EDIT_CONTENT_IN_COLUMN_BLOCK: 'signature/setEditContentInColumnBlock',
  EDIT_TEXT_BLOCK_IN_COLUMN_BLOCK: 'signature/editTextBlockInColumnBlock',
  EDIT_IMAGE_BLOCK_IN_COLUMN_BLOCK: 'signature/editTextBlockInColumnBlock',
  ADD_IMAGE_BLOCK_IN_COLUMN_BLOCK: 'signatture/addImageBlockInColumnBlock',
  CONVERT_OPS_CONTENT: 'signature/convertOpsContent',
  EDIT_HEIGHT_SPACE_BLOCK: 'signature/editHeightSpaceBlock'
};
export const editHeightSpaceBlock = (blockIndex, height) => {
  return {
    type: SignatureActions.EDIT_HEIGHT_SPACE_BLOCK,
    blockIndex,
    height
  }
}
export const addImageBlockInColumnBlock = (blockIndex, columnIndex, index, imgBase64, widths) => {
  return {
    type: SignatureActions.ADD_IMAGE_BLOCK_IN_COLUMN_BLOCK,
    blockIndex,
    columnIndex,
    index,
    imgBase64,
    widths,
  };
};
export const editImageBlockInColumnBlock = (blockIndex, columnIndex, index, key, value) => {
  return {
    type: SignatureActions.EDIT_IMAGE_BLOCK_IN_COLUMN_BLOCK,
    blockIndex,
    columnIndex,
    index,
    key,
    value,
  };
};
export const editTextBlockInColumnBlock = (parentBlockIndex, columnIndex, key, value, blockIndex, valuePdf) => {
  return {
    type: SignatureActions.EDIT_TEXT_BLOCK_IN_COLUMN_BLOCK,
    parentBlockIndex, columnIndex,
    key,
    value,
    blockIndex,
    valuePdf,
  };
};
export const setEditContentInColumnBlock = (blockIndex, columnIndex, data, index, pdfToSave) => {
  return {
    type: SignatureActions.SET_EDIT_CONTENT_IN_COLUMN_BLOCK,
    blockIndex,
    columnIndex,
    data,
    index,
    pdfToSave,
  };
};
export const removeElementInColumnBlock = (blockIndex, columnIndex, elementIndex) => {
  return {
    type: SignatureActions.REMOVE_ELEMENT_IN_COLUMN_BLOCK,
    blockIndex, columnIndex, elementIndex
  }
}
export const addElementToColumnBlock = (blockIndex, destinationColumnIndex, destinationElementIndex, blockType, dataTrans) => {
  return {
    type: SignatureActions.ADD_ELEMENT_TO_COLUMN_BLOCK,
    blockIndex,
    destinationColumnIndex,
    destinationElementIndex,
    blockType,
    dataTrans
  };
};
export const reorderElementInColumnBlock = (
  blockIndex,
  sourceColumnIndex,
  sourceElementIndex,
  destinationColumnIndex,
  destinationElementIndex
) => {
  return {
    type: SignatureActions.REORDER_ELEMENT_IN_COLUMN_BLOCK,
    blockIndex,
    sourceColumnIndex,
    sourceElementIndex,
    destinationColumnIndex,
    destinationElementIndex,
  };
};
export const selectTotalColumn = (blockIndex, total) => {
  return {
    type: SignatureActions.SELECT_TOTAL_COLUMN,
    blockIndex,
    total,
  };
};
export const changeUrlTerm = (url) => {
  return {
    type: SignatureActions.CHANGE_URL_TERM,
    url,
  };
};
export const deleteOneParty = (typeD, index) => {
  return {
    type: SignatureActions.DELETE_ONE_PARTY,
    typeD,
    index,
  };
};
export const setSignatureCurrency = (value) => {
  return {
    type: SignatureActions.SET_SIGNATURE_CURRENCY,
    value,
  };
};
export const setSignatureLanguage = (value) => {
  return {
    type: SignatureActions.SET_SIGNATURE_LANGUAGE,
    value,
  };
};
export const addMoreParter = (parent, data) => {
  return {
    type: SignatureActions.ADD_MORE_PARTER,
    parent,
    data,
  };
};
export const setRevenueType = (revenueType) => {
  return {
    type: SignatureActions.SET_REVENUETYPE,
    revenueType,
  };
};
export const changeValueShowCropPhotoModal = (value) => {
  return {
    type: SignatureActions.CHANG_VALUE_SHOW_CROP_PHOTO_MODAL,
    value,
  };
};
export const changeVatValue = (value) => {
  return {
    type: SignatureActions.CHANGE_VAT_VALUE,
    value,
  };
};

export const initDataTemplateSelected = (data) => {
  return {
    type: SignatureActions.INIT_DATA_TEMPLATE_SELECTED,
    data,
  };
};
export const uploadAttachment = (files) => {
  return {
    type: SignatureActions.UPLOAD_ATTACHMENT,
    files,
  };
};
export const uploadUrlAttachment = (url) => {
  return {
    type: SignatureActions.UPLOAD_URL_ATTACHMENTS,
    url,
  };
};
export const setObjectTypeTemplate = (objectType) => {
  return {
    type: SignatureActions.SET_OBJECT_TYPE_TEMPLATE,
    objectType,
  };
};

export const setupDataFromUserLogin = (data) => {
  return {
    type: SignatureActions.SETUP_DATA_FROM_USER_LOGIN,
    data,
  };
};

export const setupDataFromContact = (data) => {
  return {
    type: SignatureActions.SETUP_DATA_FROM_CONTACT,
    data,
  };
};
export const setupDataFromCompany = (data) => {
  return {
    type: SignatureActions.SETUP_DATA_FROM_COMPANY,
    data,
  };
};
export const resetDefaultData = () => {
  return {
    type: SignatureActions.RESET_DEFAULT_DATA,
  };
};
export const setPositionBlockLastestFocused = (index, parentBlockIndex, parentColumnIndex) => {
  return {
    type: SignatureActions.SET_POSITION_BLOCK_LASTEST_FOCUSED,
    index,
    parentBlockIndex,
    parentColumnIndex
  };
};
export const setDataFillToEditor = (data) => {
  return {
    type: SignatureActions.SET_DATA_FILL_TO_EDITOR,
    data,
  };
};
export const setEditContent = (data, index, pdfToSave) => {
  return {
    type: SignatureActions.SET_EDIT_CONTENT,
    data,
    index,
    pdfToSave,
  };
};
export const toggleColumnProductBlock = (key) => {
  return {
    type: SignatureActions.TOGGLE_COLUMN_PRODUCT_BLOCK,
    key,
  };
};
export const setupDataParterBlock = (data) => {
  return {
    type: SignatureActions.SETUP_DATA_PARTER_BLOCK,
    data,
  };
};

export const editParterBlock = (parent, data, index) => {
  return {
    type: SignatureActions.EDIT_PARTER_BLOCK,
    parent,
    data,
    index,
  };
};
export const setProductData = (data) => {
  return {
    type: SignatureActions.SET_PRODUCT_DATA,
    data,
  };
};
export const editCover = (key, value, valuePdf, blockIndex) => {
  return {
    type: SignatureActions.EDIT_COVER,
    key,
    value,
    valuePdf,
    blockIndex,
  };
};

export const editTextBlock = (key, value, blockIndex, valuePdf) => {
  return {
    type: SignatureActions.EDIT_TEXT_BLOCK,
    key,
    value,
    blockIndex,
    valuePdf,
  };
};
export const editHeader = (key, value, valuePdf) => {
  return {
    type: SignatureActions.EDIT_HEADER,
    key,
    value,
    valuePdf,
  };
};
export const addImageBlock = (index, imgBase64, widths) => {
  return {
    type: SignatureActions.ADD_IMAGE_BLOCK,
    index,
    imgBase64,
    widths,
  };
};
export const editImageBlock = (index, key, value) => {
  return {
    type: SignatureActions.EDIT_IMAGE_BLOCK,
    index,
    key,
    value,
  };
};
export const selectMode = (mode) => {
  return {
    type: SignatureActions.SELECT_MODE,
    mode,
  };
};
export const setupDataFromDeal = (data) => {
  return {
    type: SignatureActions.SETUP_DATA_FROM_DEAL,
    data,
  };
};
export const setVisibleRightMenu = (visible, blockType, blockIndex) => {
  return {
    type: SignatureActions.SET_VISIBLE_RIGHT_MENU,
    visible,
    blockType,
    blockIndex,
  };
};

export const reorderElement = (oldIndex, newIndex) => {
  return {
    type: SignatureActions.REORDER_ELEMENT,
    oldIndex,
    newIndex,
  };
};

export const addBlock = (blockType, index, dataTrans) => {
  return {
    type: SignatureActions.ADD_BLOCK,
    blockType,
    index,
    dataTrans,
  };
};

export const removeBlock = (index) => {
  return {
    type: SignatureActions.REMOVE_BLOCK,
    index,
  };
};

export const editBlock = (index, data) => {
  return {
    type: SignatureActions.EDIT_BLOCK,
    index,
    data,
  };
};
export const isUploadPDF = (index) => {
  return {
    type: SignatureActions.IS_UPLOAD_PDF,
    index,
  };
};
export const convertOpsContent = (index, key, value) => {
  return {
    type: SignatureActions.CONVERT_OPS_CONTENT,
    index,
    key,
    value
  }
}
