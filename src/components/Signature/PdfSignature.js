/* eslint-disable */
import React from 'react';
import _l from 'lib/i18n';
import { Page, Text, View, Document, StyleSheet, Image, Font, Link } from '@react-pdf/renderer';
import Open_Sans from "../../font/Open_Sans/OpenSans-Regular.ttf"
import Open_Sans_Bold from "../../font/Open_Sans/OpenSans-Bold.ttf"
import  eSignBgDefault from '../../../public/eSignBgDefaut.jpg';
import attachIcon from '../../../public/attach.png'
import { forEach } from 'lodash';
import { REVENUETYPE } from '../../Constants';
import moment from 'moment';
import pdfIcon from '../../../public/pdfNew.png';
import docxIcon from '../../../public/google-docs-color.png'
import sheetIcon from '../../../public/google-sheets-color.png'
import presentationIcon from '../../../public/google-slides-color.png'

Font.register({
  family: 'Open_Sans',
  src: Open_Sans
})

Font.register({
  family: 'Open_Sans_Bold',
  src: Open_Sans_Bold
})

Font.registerHyphenationCallback(word => (
  [word]
));

const ratio = 1240/794;
const styles = StyleSheet.create({
  page: {backgroundColor: 'white', fontSize: 16, fontFamily: "Open_Sans"},
  section: { color: 'white', textAlign: 'center', margin: 30 },
  leftH: {width: "30%", alignItems: 'center'},
  centerH: {width: "40%", textAlign: 'center'},
  rightH: {width: "30%", textAlign: 'center'},
  nameH: {textAlign: 'center', fontWeight: '400', fontSize: '25'},
  textCenter: {textAlign: 'center'},
  avatar: { width: "300px", height: "300px" },
  avatarC: {padding: "40px 20px"},
  bulletPoint: {
    width: 5,
    height: 5,
    backgroundColor: "#000",
    fontSize: 20,
    marginRight: "10",
    marginTop: "4"
  },
  inline: {flexDirection: 'row'},
  consultantN: {textAlign: 'center', fontSize: '30', marginTop: '20'},
  newspaper: {
    flexDirection: 'row',
    paddingLeft: "20%"
  },
  section: {
    width: "40%",
    margin: 10,
    padding: 10,
    flexGrow: 1
  },
  fontBold: {fontFamily: 'Open_Sans_Bold'},
  header: { marginBottom: '50'},
  textHighlight: { fontSize: '20' },
  nameHighlight: {fontSize: '50', maxLines: "2"},
  consultant: {width: '300', textAlign: 'center', fontSize: '30', margin: '10px 0'},
  row: {flexDirection: 'row', margin: '10px 0'},
  col4One: {width: '20%'},
  col4Two: {width: '40%', padding: '5px'},
  col4Three: {width: '20%'},
  col4Four: {width: '20%'},
  col3One: {width: '20%'},
  col3Two: {width: '40%', padding: '5px'},
  col3Three: {width: '40%'},
  titlePage3: {width: '20%', textTransform: 'uppercase'},
  footer: {
    flexDirection: 'row',
    position: 'absolute',
    bottom: 15,
    right: 0,
    paddingRight: 36,
  },
  th: {
    flex: 1, borderRight: true, borderColor: '#e0e0e0', fontFamily:  'Open_Sans_Bold', padding: 10
  },
  td: {
    flex: 1, borderRight: true, borderColor: '#e0e0e0', padding: 10
  },
  verticalCenter : {
    position: 'absolute',
    top: '50%',
    transform: 'translateY(-50%)',
  },
  attachmentBox: {
    display: 'flex', 
    flexDirection: 'row',
    // borderColor: '#f0f0f0',
    // borderTop: 1,
    // borderRight: 1,
    // borderBottom: 1, 
    // borderLeft: 1,
    // borderStyle: 'solid',
    margin: '5px 0', 
    padding: 10, 
    borderRadius: 4, 
    alignItems: 'center'
    // justifyContent: 'space-between' 
  }
});

const LAYOUTS = {
  Layout1: 'layout1',
  Layout2: 'layout2',
  Layout3: 'layout3',
  Layout4: 'layout4',
  Layout5: 'layout5',
  Layout6: 'layout6'
}

const TextHeaderDefault = (e) => {
  return <Text style={{fontFamily: 'Open_Sans_Bold', padding: '125px 0', fontSize: 40 }}>{_l`Proposal`.toUpperCase()}</Text>
}

const ParterBlock = (__PARTER_BLOCK_DATA) => {
  return <Text></Text>
  // return  <View>
  //   <Text>
  //     {`Prepared for ${(__PARTER_BLOCK_DATA?.__COMPANY_CONTACT_INFO?.[0]?.companyName?.value !== '' &&
  //     __PARTER_BLOCK_DATA?.__COMPANY_CONTACT_INFO?.[0]?.companyName?.value) ||
  //   __PARTER_BLOCK_DATA?.__COMPANY_CONTACT_INFO?.[0]?.fullName?.value ||
  //   ''}`}</Text>
  //   <Text>{`By ${__PARTER_BLOCK_DATA?.__USER_INFO?.[0]?.fullName?.value || ''}`}</Text>
  // </View>
}

const Header = (e, __PARTER_BLOCK_DATA) => {
  let _widthImage = e?.imageMainCoverSize ? (e?.imageMainCoverSize.width/ 794 ) * 1240 : '40%';
  let _heightImage = e?.imageMainCoverSize ? (e?.imageMainCoverSize.height/1123) * 1754 : '100%';
  switch (e.layout) {
    case LAYOUTS.Layout1:
      return (
      <View style={{flexDirection: 'row', minHeight: 624}}>
        {e.backgroundFilter ? <View style={{ width: '40%', backgroundColor: e.backgroundFilter}}></View> : <Image style={{width: _widthImage, height: _heightImage}} cache={false} source={e.background ? e.background : e.background ? e.background : eSignBgDefault}></Image>}
        <View style={{width: '60%',flexDirection: 'column', paddingLeft: '20px', backgroundColor: e.backgroundColor ? e.backgroundColor: 'white'}}>
          <View style={{ paddingRight: 10, flex: 2, justifyContent: "flex-end"}}>
            <View style={{ width: (1240 - _widthImage), padding: '30px 50px' }}>
            {e?.contentPdf?.ops ? convertOpsToPdf(e?.contentPdf?.ops) : TextHeaderDefault(e)}
            </View>
          </View>
        </View>
      </View>
      )
    case LAYOUTS.Layout2:
      return (
      <View style={{flexDirection: 'row', minHeight: 624}}>
         <View style={{ flex: 1,flexDirection: 'column', paddingLeft: '20px', backgroundColor: e.backgroundColor ? e.backgroundColor: 'white'}}>
         {/*{*/}
         {/*   e?.logo ?*/}
         {/*   <View style={{paddingLeft: 10, marginTop: 10, height: 162/1.2, width: 347/2.2}}><Image style={{width: '100%', height: '100%'}} cache={false} source={e?.logo ? e.logo : ''}></Image></View>*/}
         {/*   : <Text></Text>*/}
         {/* }*/}
         <View style={{ paddingRight: 10, flex: 2, justifyContent: 'center'}}>
            <View style={{ width: (1180 - _widthImage) }}>
            {e?.contentPdf?.ops ?
              // convertOpsToPdf(e?.contentPdf?.ops) :
              // <Text style={{fontFamily: 'Open_Sans_Bold', padding: e?.logo ? '0 0 20px 0' : '125px 0' }}>{_l`Proposal`.toString().toUpperCase()}</Text>}
              convertOpsToPdf(e?.contentPdf?.ops) : TextHeaderDefault(e)}
            </View>
          </View>
          <View style={{ flex: 1, justifyContent: 'center'}}>
            {ParterBlock(__PARTER_BLOCK_DATA)}
          </View>
        </View>
        {e.backgroundFilter ? <View style={{ width: '40%', backgroundColor: e.backgroundFilter}}></View> : <Image style={{width: _widthImage, height: _heightImage}} cache={false} source={e.background ? e.background : e.background ? e.background : eSignBgDefault}></Image>}
      </View>);
    case LAYOUTS.Layout3:
      return (
      <View>
        <View style={{width: '100%', padding: '0px 125px'}}>
          {e.backgroundFilter ? <View style={{ width: '100%', height: '100%', backgroundColor: e.backgroundFilter}}></View> : <Image style={{width: _widthImage || '100%', height: _heightImage}} cache={false} source={e.background ? e.background : eSignBgDefault}></Image>}
        </View>
        {
            e?.logo ?
            <View style={{paddingLeft: 10, marginTop: 10, height: 162/1.2, width: 347/2.2}}><Image style={{width: '100%', height: '100%'}} cache={false} source={e?.logo ? e.logo : ''}></Image></View>
            : <Text></Text>
          }
        <View style={{paddingLeft: '20px', backgroundColor: e.backgroundColor ? e.backgroundColor: 'white'}}>
        <View style={{padding: '50px 100px 10px 100px'}}>{e?.contentPdf?.ops ? convertOpsToPdf(e?.contentPdf?.ops) : TextHeaderDefault(e)}</View>
          {ParterBlock(__PARTER_BLOCK_DATA)}
        </View>
      </View>);
    case LAYOUTS.Layout4:
      return (
      <View>
        <View style={{paddingLeft: '20px', paddingBottom: 20 ,backgroundColor: e.backgroundColor ? e.backgroundColor: 'white'}}>
          {/*{*/}
          {/*  e?.logo ?*/}
          {/*  <View style={{paddingLeft: 10, marginTop: 10, height: 162/1.2, width: 347/2.2}}><Image style={{width: '100%', height: '100%'}} cache={false} source={e?.logo ? e.logo : ''}></Image></View>*/}
          {/*  : <Text></Text>*/}
          {/*}*/}
          <View style={{padding: '50px 100px 10px 100px'}}>{e?.contentPdf?.ops ? convertOpsToPdf(e?.contentPdf?.ops) : TextHeaderDefault(e)}</View>
          {ParterBlock(__PARTER_BLOCK_DATA)}
        </View>
        <View style={{width: '100%', padding: '0px 125px'}}>
          {e.backgroundFilter ? <View style={{ width: '100%', height: _heightImage, backgroundColor: e.backgroundFilter}}></View> : <Image style={{width: _widthImage || '100%', height: _heightImage}}cache={false} source={e.background ? e.background : eSignBgDefault}></Image>}
        </View>
      </View>);
      case LAYOUTS.Layout5:
      case LAYOUTS.Layout6:
        return (
        <View>
          <View style={{width: '100%', minHeight: 624, height: _heightImage}}>
            {e.backgroundFilter ? <View style={{ width: '100%', height: '100%', backgroundColor: e.backgroundFilter}}></View> : <Image style={{width: _widthImage, height: _heightImage}} cache={false} source={e.background ? e.background : eSignBgDefault}></Image>}
          </View>
          {/*{*/}
          {/*  e?.logo ?*/}
          {/*  <View style={{paddingLeft: 10, marginTop: 10, height: 162/1.2, width: 347/2.2}}><Image style={{width: '100%', height: '100%'}} cache={false} source={e?.logo ? e.logo : ''}></Image></View>*/}
          {/*  : <Text></Text>*/}
          {/*}*/}
          <View style={{paddingLeft: '20px', backgroundColor: e.backgroundColor ? e.backgroundColor: 'white'}}>
          <Text style={{padding: '10px 0 20px 0'}}>{e?.contentPdf?.ops ? convertOpsToPdf(e?.contentPdf?.ops) : TextHeaderDefault(e)}</Text>
            {ParterBlock(__PARTER_BLOCK_DATA)}
          </View>
        </View>
        );
  }
}

const Parter = (e, __PARTER_BLOCK_DATA) => {
  return (
    <View wrap={false} style={{ padding: '0 125px' }}>
    <View style={{padding: '10px 0'}}>
      <Text style={{fontFamily: 'Open_Sans_Bold', fontSize: 30.5, marginBottom: 10}}>{_l`Parties`}</Text>
      <Text style={[styles.fontBold, {marginBottom: 10}]}>01 - {_l`To`}</Text>
    </View>
    {__PARTER_BLOCK_DATA?.__COMPANY_CONTACT_INFO?.map((item, index) => {
      return(
        <View style={{padding: '5px 0'}} wrap={false}>
            <View style={{flexDirection: 'row', border: true, borderColor: '#e0e0e0', borderBottom: false}}>
              <View style={{flex: 1, borderRight: true, borderColor: '#e0e0e0'}}>
                <Text style={{padding: 5}}>{_l`Company`}</Text>
                <Text style={{fontFamily: "Open_Sans_Bold", padding: 5}}>{item?.companyName?.value}</Text>
              </View>
              <View style={{flex: 1}}>
                <Text style={{padding: 5}}>{_l`Contact name`}</Text>
                <Text style={{fontFamily: "Open_Sans_Bold", padding: 5}}>{item?.fullName?.value}</Text>
              </View>
            </View>

            <View style={{flexDirection: 'row', border: true, borderColor: '#e0e0e0', borderBottom: false}}>
              <View style={{flex: 1, borderRight: true, borderColor: '#e0e0e0'}}>
                <Text style={{padding: 5}}>{_l`VAT number`}</Text>
                <Text style={{fontFamily: "Open_Sans_Bold", padding: 5}}>{item?.vatNumber?.value}</Text>
              </View>
              <View style={{flex: 1}}>
                <Text style={{padding: 5}}>{_l`Contact email`}</Text>
                <Text style={{fontFamily: "Open_Sans_Bold", padding: 5}}>{item?.email?.value}</Text>
              </View>
            </View>

            <View style={{flexDirection: 'row', border: true, borderColor: '#e0e0e0', borderBottom: false}}>
              <View style={{flex: 1, borderRight: true, borderColor: '#e0e0e0'}}>
                <Text style={{padding: 5}}>{_l`Company address`}</Text>
                <Text style={{fontFamily: "Open_Sans_Bold", padding: 5}}>{item?.address?.value}</Text>
              </View>
              <View style={{flex: 1}}>
                <Text style={{padding: 5}}>{_l`Contact phone`}</Text>
                <Text style={{fontFamily: "Open_Sans_Bold", padding: 5}}>{item?.phone?.value}</Text>
              </View>
            </View>

            <View style={{flexDirection: 'row', border: true, borderColor: '#e0e0e0'}}>
              <View style={{flex: 1, borderRight: true, borderColor: '#e0e0e0'}}>
                <Text style={{padding: 5}}>{_l`Title`}</Text>
                <Text style={{fontFamily: "Open_Sans_Bold", padding: 5}}>{item?.title?.value}</Text>
              </View>
              <View style={{flex: 1}}>
              </View>
            </View>

          </View>
        )
      })
    }
    {
      __PARTER_BLOCK_DATA?.__USER_INFO?.map((item) => {
        return (
          <View style={{marginTop: 10, padding: '20px 0'}} wrap={false}>
          <Text style={{fontFamily: 'Open_Sans_Bold', marginBottom: 10}}>02 - {_l`From`}</Text>
          <View style={{flexDirection: 'row', border: true, borderColor: '#e0e0e0', borderBottom: false}}>
            <View style={{flex: 1, borderRight: true, borderColor: '#e0e0e0'}}>
              <Text style={{padding: 5}}>{_l`Company`}</Text>
              <Text style={{fontFamily: "Open_Sans_Bold", padding: 5}}>{item?.companyName?.value}</Text>
            </View>
            <View style={{flex: 1}}>
              <Text style={{padding: 5}}>{_l`Name`}</Text>
              <Text style={{fontFamily: "Open_Sans_Bold", padding: 5}}>{item?.fullName?.value}</Text>
            </View>
          </View>

          <View style={{flexDirection: 'row', border: true, borderColor: '#e0e0e0', borderBottom: false}}>
            <View style={{flex: 1, borderRight: true, borderColor: '#e0e0e0'}}>
              <Text style={{padding: 5}}>{_l`VAT number`}</Text>
              <Text style={{fontFamily: "Open_Sans_Bold", padding: 5}}>{item?.vatNumber?.value}</Text>
            </View>
            <View style={{flex: 1}}>
              <Text style={{padding: 5}}>{_l`Email`}</Text>
              <Text style={{fontFamily: "Open_Sans_Bold", padding: 5}}>{item?.email?.value}</Text>
            </View>
          </View>

          <View style={{flexDirection: 'row', border: true, borderColor: '#e0e0e0', borderBottom: false}}>
            <View style={{flex: 1, borderRight: true, borderColor: '#e0e0e0'}}>
              <Text style={{padding: 5}}>{_l`Address`}</Text>
              <Text style={{fontFamily: "Open_Sans_Bold", padding: 5}}>{item?.address?.value}</Text>
            </View>
            <View style={{flex: 1}}>
              <Text style={{padding: 5}}>{_l`Phone`}</Text>
              <Text style={{fontFamily: "Open_Sans_Bold", padding: 5}}>{item?.phone?.value}</Text>
            </View>
          </View>

          <View style={{flexDirection: 'row', border: true, borderColor: '#e0e0e0'}}>
            <View style={{flex: 1, borderRight: true, borderColor: '#e0e0e0'}}>
              <Text style={{padding: 5}}>{_l`Title`}</Text>
              <Text style={{fontFamily: "Open_Sans_Bold", padding: 5}}>{item?.title?.value}</Text>
            </View>
            <View style={{flex: 1}}>
            </View>
          </View>
        </View>
        )
      })
    }
    </View>
  )
}


const Pricing = (__SETTING_COLUMN_PRODUCT, __PRODUCT, __REVENUETYPE, __TOTAL_VALUE_PRODUCT, currency) => {
  const lengthProduct = __PRODUCT.length - 1;
  return (
    <View style={{padding: '20px 125px'}} wrap={false}>
      <Text style={{fontFamily: 'Open_Sans_Bold', fontSize: 30.5, marginBottom: 20}}>{_l`Scope of work`}</Text>
      {/* <Text style={{fontFamily: 'Open_Sans_Bold', fontSize: 11}}>{_l`Services / Products`}</Text> */}
      <View style={{flexDirection: 'row', border: true, borderColor: '#e0e0e0', borderBottom: false, backgroundColor: '#f0f0f0'}}>
        {__SETTING_COLUMN_PRODUCT.productGroup && <View style={styles.th}><Text>{_l`Product group`}</Text></View>}
        {__SETTING_COLUMN_PRODUCT.product && <View style={styles.th}><Text>{_l`Product`}</Text></View>}
        {__SETTING_COLUMN_PRODUCT.productType && <View style={styles.th}><Text>{_l`Product type`}</Text></View>}
        {__SETTING_COLUMN_PRODUCT.unitAmount && <View style={styles.th}><Text>{_l`Unit amount`}</Text></View>}
        {__SETTING_COLUMN_PRODUCT?.startDate && __REVENUETYPE !== REVENUETYPE.FIXED_RECURRING && (
          <View>{_l`Start date`}</View>
        )}
        {__SETTING_COLUMN_PRODUCT?.endDate && __REVENUETYPE !== REVENUETYPE.FIXED_RECURRING && (
          <View>{_l`End date`}</View>
        )}
        {__SETTING_COLUMN_PRODUCT.unitPrice && <View style={styles.th}><Text>{_l`Price per unit`}</Text></View>}
        {__SETTING_COLUMN_PRODUCT.unitCost && <View style={styles.th}><Text>{_l`Unit cost`}</Text></View>}
        {__SETTING_COLUMN_PRODUCT.discountPercent && <View style={styles.th}><Text>{_l`Discount %`}</Text></View>}
        {__SETTING_COLUMN_PRODUCT.discountPrice && <View style={styles.th}><Text>{_l`Discounted price`}</Text></View>}
        {__SETTING_COLUMN_PRODUCT.discountAmount && <View style={styles.th}><Text>{_l`Discount amount`}</Text></View>}
        {__SETTING_COLUMN_PRODUCT.margin && <View style={styles.th}><Text>{_l`Margin %`}</Text></View>}
        {__SETTING_COLUMN_PRODUCT.cost && <View style={styles.th}><Text>{_l`Cost`}</Text></View>}
        {__SETTING_COLUMN_PRODUCT.value && <View style={styles.th}><Text>{_l`Value`}</Text></View>}
        {__SETTING_COLUMN_PRODUCT.profit && <View style={styles.th}><Text>{_l`Profit`}</Text></View>}
      </View>

      <View>
      {__PRODUCT.map((e, index) => {
            return (
              <View key={index} style={{flexDirection: 'row', border: true, borderBottom: index === lengthProduct ? true : false,  borderColor: '#e0e0e0'}}>
                {__SETTING_COLUMN_PRODUCT.productGroup && <View style={styles.td}><Text>{e?.lineOfBusinessName}</Text></View> }
                {__SETTING_COLUMN_PRODUCT.product && <View style={styles.td}><Text>{e.productDTO?.name}</Text></View>}
                {__SETTING_COLUMN_PRODUCT.productType && <View style={styles.td}><Text>{e.measurementTypeName}</Text></View>}
                {__SETTING_COLUMN_PRODUCT?.startDate && __REVENUETYPE !== REVENUETYPE.FIXED_RECURRING && (
                  <View>{e.deliveryStartDate ? mome(e.deliveryStartDate).format('DD MMM, YYYY') : ''}</View>
                )}
                {__SETTING_COLUMN_PRODUCT?.endDate && __REVENUETYPE !== REVENUETYPE.FIXED_RECURRING && (
                  <View>{e.deliveryEndDate ? moment(e.deliveryEndDate).format('DD MMM, YYYY') : ''}</View>
                )}
                {__SETTING_COLUMN_PRODUCT.unitAmount && <View style={styles.td}><Text>{e.numberOfUnit}</Text></View>}
                {__SETTING_COLUMN_PRODUCT.unitPrice && <View style={styles.td}><Text>{e.price}</Text></View>}
                {__SETTING_COLUMN_PRODUCT.unitCost && <View style={styles.td}><Text>{e.costUnit}</Text></View>}
                {__SETTING_COLUMN_PRODUCT.discountPercent && <View style={styles.td}><Text>{e.discountPercent}</Text></View>}
                {__SETTING_COLUMN_PRODUCT.discountPrice && <View style={styles.td}><Text>{e.discountedPrice}</Text></View>}
                {__SETTING_COLUMN_PRODUCT.discountAmount && <View style={styles.td}><Text>{e.discountAmount}</Text></View>}
                {__SETTING_COLUMN_PRODUCT.margin && <View style={styles.td}><Text>{e.margin}</Text></View>}
                {__SETTING_COLUMN_PRODUCT.cost && <View style={styles.td}><Text>{e.cost}</Text></View>}
                {__SETTING_COLUMN_PRODUCT.value && <View style={styles.td}><Text>{e.value}</Text></View>}
                {__SETTING_COLUMN_PRODUCT.profit && <View style={styles.td}><Text>{e.profit}</Text></View>}
              </View>
            );
          })}
      </View>
      <View style={{flexDirection: 'row', marginTop: 10}}>
      <View style={{width: "60%"}}></View>
      <View right style={{width: '40%'}}>
        <Text style={styles.fontBold}>{_l`Price summary`}</Text>
        <View style={{flexDirection: 'row', marginTop: 20, marginBottom: 10}}>
          <Text style={{flex: 2}}>Net</Text>
          <Text style={{flex: 1, textAlign: 'right'}}>{__TOTAL_VALUE_PRODUCT?.net || 0} {currency}</Text>
        </View>
        <View style={{flexDirection: 'row', borderBottom: true, borderColor: '#e0e0e0', marginBottom: 10 }}>
          <Text style={{flex: 2}}>{_l`VAT`} (%)</Text>
          <Text style={{flex: 1, textAlign: 'right'}}>{__TOTAL_VALUE_PRODUCT?.vat || 0}</Text>
        </View>
        <View style={{flexDirection: 'row', marginTop: 10}}>
          <Text style={{flex: 2}}> {_l`Total`} </Text>
          <Text style={{flex: 1, textAlign: 'right'}}>{__TOTAL_VALUE_PRODUCT?.total || 0} {currency}</Text>
        </View>
      </View>
      </View>
    </View>
  )
}

const Term = (e) => {
  return (
    <View style={{flexDirection: 'row', textAlign: 'center', height: 150}}>
      <Text style={[styles.fontBold, {padding:'65 0'}]}>{_l`By approving this document you agree with the`} <Link style={{ textDecoration: 'none' }} src={e?.urlTerm ? e.urlTerm : ''} style={{color: "#4183c4", fontFamily: "Open_Sans_Bold"}}> {_l`terms and conditions.`}</Link></Text>
    </View>
  )
}
const ImageBlock = (e) => {
  let _width =  e?.width ? (e?.width/ 794 ) * 1240 : 'auto';
  return e.img ? (
    <View style={{maxHidth: '100%', padding: 125}}>
      <View style={{display: 'block', marginLeft: e.align !== 'left' ?  'auto' : 0, marginRight: e.align !== 'right' ? 'auto' : 0}}>
        <Image style={{width: _width}} source={e.img}/>
      </View>
    </View>
  ) : (null)
}

const getStyleText = (attributes) => {
  let _style = attributes;
  if(attributes?.bold) {
    _style['fontFamily'] =  'Open_Sans_Bold'
  }
  if(attributes?.background) {
    _style['backgroundColor'] = attributes?.background ;
  }
  if(attributes?.header === 3 || attributes?.header === 2 || attributes?.header === 1) {
    _style['fontFamily'] =  'Open_Sans_Bold'
  }

  return _style ? _style : {}
}
const getFontSize = (size) => {
  switch(size) {
    case 1:
      return 40;
    case 2:
      return 30.5
    case 3:
      return 23.8
    default:
     return 17
  }
}
const getStyleRow = (style) => {
  return {
    ...style,
    flexDirection: 'row',
    textAlign: style?.align,
    fontFamily: style?.header === 3 || style?.header === 2 || style?.header === 1 ? 'Open_Sans_Bold' : 'Open_Sans',
    fontSize: getFontSize(style?.header),
    lineHeight: 1.4285
  }

}

const addRowToArray = (opsNext, op) => {
  let array = [];
  let _filterBreak = op.insert.split('_break')
  forEach(_filterBreak, (_breakLine) => {
    array.push({row: [{ insert: _breakLine, attributes: _breakLine.length === 0 ? {  minHeight: 10, height: !opsNext?.insert?.match(/\n/g) ? (op?.attributes?.header ? getFontSize(op?.attributes?.header)-12 : 16) : 0 } : op.attributes }], style: _breakLine.length === 0 ? {  minHeight: 10, height: !opsNext?.insert?.match(/\n/g) ? (op?.attributes?.header ? getFontSize(op?.attributes?.header)-12 : 16) : 0} : {}});
  })
  return array;
}


const convertOpsToPdf = (ops) => {
  let array = [];
  let row = []
  forEach(ops, (_op, index) => {
    let op ={}
    if(typeof _op.insert !== "object"){
      op = {
        ..._op,
        insert: _op?.insert.replaceAll(/\n/g, '_break')
      }
    } else {
      op={
        ..._op,
        insert: _op?.insert?.image,
        img: _op?.insert?.image
      }
    }

    if(op.insert !== '_break') {
      // if(op.insert.substr(-5) === 'break' || op.insert.substr(1, 5) === 'break') {
        if(op.insert.indexOf('_break') !== -1) {
        array.push({row: row, style: op.attributes});
        array = [...array, ...addRowToArray(ops[index+1], op)]
        row = [];
      } else {
        //add new row
          row.push(op)
      }
    } else {
      array.push({row: row, style: op.attributes})
      if(index === 0 || (op?.attributes?.header && ops[index+1]?.insert === '\n')){
        array = [...array, ...addRowToArray(ops[index+1], op)]
      }
      row = [];
    }
  });

  let _data = array.map((item) => {
    if(item?.row?.filter(e => e.img).length !== 0){
      return (
        item?.row?.filter(e => e.img).map((image) => {
          console.log("this is an image url", image.img)
          return <Image src={image.img} cache={false}/>
        })
      )
    } else {
      return (
        <Text style={getStyleRow(item.style)}>
          {item?.row?.map((text) => {
            return <Text style={getStyleText(text.attributes)}>{text.insert}</Text>;
          })}
        </Text>
      )
    }
  })
  return _data;
}

const Cover = (ops) => {

  return convertOpsToPdf(ops)
}

const BlockText = (item) => {
//     return <View style={{minHeight: 100, padding: 30, color: "black"}}>{ item?.pdfToSave?.ops ? convertOpsToPdf(item?.pdfToSave?.ops): <Text style={{color: "black"}}>Title</Text>}</View>
// }
    return <View style={{minHeight: 300, padding: '78px 125px', color: "black"}}>{ item?.valuePdf?.ops ? convertOpsToPdf(item?.valuePdf?.ops): <Text style={{color: "black", fontSize: 26, fontFamily: 'Open_Sans_Bold'}}>{_l`Title`}</Text>}</View>
}

const SpaceBlock = (item) => {
  return <View style={{minHeight: item?.height || 100, height: item?.height || 100}}></View>
}

const getContentColumnBlock = (block) => {
  switch (block.type) {
    case 'TEXT':
      return (
        <View style={{width: '100%'}}>
          <View>{convertOpsToPdf(block?.valuePdf?.ops)}</View>
        </View>
      )
    case 'IMAGE':
      if(block.img) {
        return (
          <Image cache={false} source={block.img}/>
        )
      }
      return <View></View>

  }
}

const ColumnBlock = (item) => {
console.log("🚀 ~ file: PdfSignature.js ~ line 530 ~ ColumnBlock ~ item", item)
  let _length = item?.__ELEMENTS?.length
  let _widthColumn = _length > 0  ? (_length === 3 ? 100 - 4*2 : 100 - (_length-1)*3)/ _length : 100
  return (
    <View style={{ flexDirection: 'row', padding: '50px 125px' }}>
      
      {
        item?.__ELEMENTS.map((block) => {
          return (
            <View style={{ width: `${_widthColumn}%`, marginRight: 20, paddingTop: 20 }}>
              {
                block.map((content) => {
                  return (
                    getContentColumnBlock(content)
                  )
                })
              }
            </View>
          )
        })
      }
    </View>
  )
}

const getAttachmentIcon = (type, url) => {
  switch (type) {
    case 'pdf':
      return pdfIcon
    case 'xlsx':
      return sheetIcon
    case 'docx':
    case 'doc':
      return docxIcon
    // case 'svg':
    case 'png':
    case 'jpg':
    case 'jpeg':
    case 'JPG':
      return url
    case 'ppt':
    case 'pptx':
      return presentationIcon
    default:
      return attachIcon
  }
}
const supportTypeIcon = ['pdf', 'xlsx', 'docx', 'doc', 'png', 'jpeg', 'jpg', 'ppt', 'pptx']
const AttachmentBlock = (item) => {
  return (
    <View style={{ flexDirection: 'column', padding: '50px 125px' }}>
      <Text style={{ fontFamily: 'Open_Sans_Bold', fontSize: 30.5, marginBottom: 10 }}>{_l`Attachments`}</Text>
      {item?.attachmentsUrl?.map(e => {
        let name = e.match(/\/([^\/?#]+)[^\/]*$/)[1].split(/\_(?=[^\.]+$)/)[0]
        let isSupportedIcon = supportTypeIcon.some(i => name.substr(name.lastIndexOf('.') + 1).includes(i))
        console.log(name, " is ", isSupportedIcon)
        return (
          <View style={styles.attachmentBox}>
            <View style={{ display: 'flex', justifyContent: 'flex-start', alignItems: 'center' }}>
              <Image cache={false} source={getAttachmentIcon(name.substr(name.lastIndexOf('.') + 1), e)} style={{ width: isSupportedIcon ? 40 : 30, marginRight: isSupportedIcon ? 0: 10 }} />
            </View>
            {
              name.length < 90 ? (
                <Text style={{ fontFamily: 'Open_Sans', fontSize: 17, padding: '0px 12px' }}>{name}</Text>)
                : (
                  <View style={{ flexDirection: 'column', padding: '0px 12px' }}>
                    <Text style={{ fontFamily: 'Open_Sans', fontSize: 17 }}>{name.substr(0, 91)}</Text>
                    { name.length <= 196
                      ? <Text style={{ fontFamily: 'Open_Sans', fontSize: 17 }}>{name.substr(92, name.length)}</Text>
                      : <Text style={{ fontFamily: 'Open_Sans', fontSize: 17 }}>{`${name.substr(92, 197)}...`}</Text>
                    }
                  </View>
                )
            }
              
          </View>
        )
      })}
    </View>
  )
}
const PdfSignature = ({
  __ELEMENTS,
  __SETTING_COLUMN_PRODUCT,
  __PRODUCT,
  __REVENUETYPE,
  __TOTAL_VALUE_PRODUCT,
  currency,
  __PARTER_BLOCK_DATA,
}) => {
  const handelRenderElement = (item) => {
    switch (item.type) {
      case 'HEADER':
        return Header(item, __PARTER_BLOCK_DATA)
      case 'PARTER':
        return Parter(item, __PARTER_BLOCK_DATA)
      case 'PRICING':
        return Pricing(__SETTING_COLUMN_PRODUCT, __PRODUCT, __REVENUETYPE, __TOTAL_VALUE_PRODUCT, currency)
      case 'TERM':
        return Term(item)
      case 'COVER':
        return <View wrap={false} style={{ backgroundColor: item.backgroundColor ? item.backgroundColor : 'white', minHeight: 312, padding: '78px 125px'}}>{item?.valuePdf?.ops ? Cover(item?.valuePdf?.ops) : <Text style={{color: "white", textAlign: "center", fontSize: 40, fontFamily: 'Open_Sans_Bold'}}>{_l`An interesting title`}</Text>}</View>
      case 'TEXT':
        return BlockText(item)
      case 'IMAGE':
        return ImageBlock(item)
      case 'COLUMN':
        return ColumnBlock(item)
      case 'SPACE':
        return SpaceBlock(item)
      case 'ATTACHMENT':
        return AttachmentBlock(item)
    }
  }

return (
<Document>
    <Page size={{width: 1240, height: 1754}} style={styles?.page}>
      <View fixed style={{ margin: '10px 40px' }}>
        <Text style={{ textAlign: 'right' }} render={({ pageNumber, totalPages }) => (
          `${_l`Page`} ${pageNumber} / ${totalPages}`
        )} fixed />
      </View>
      {__ELEMENTS.map((item) => (
        handelRenderElement(item)
      ))}
  	</Page>

  </Document>
  );
}

export default PdfSignature;
