import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import style from '../Signature.css';
import _l from 'lib/i18n';
import cx from 'classnames';
import GroupControllerButton from './GroupControllerButton';
import { BLOCKTYPE_SIGNATURE, REVENUETYPE, Endpoints } from '../../../Constants';
import { Button, Table, Input, Divider, Grid, GridRow, GridColumn } from 'semantic-ui-react';
import SettingProductColumn from '../SettingProductColumn';
import { changeVatValue } from '../signature.actions';
import api from 'lib/apiClient';
import moment from 'moment';

export const PricingService = (props) => {
  const {
    __PRODUCT,
    __SETTING_COLUMN_PRODUCT,
    __MODE,
    currency,
    __TOTAL_VALUE_PRODUCT,
    changeVatValue,
    __REVENUETYPE,
    vat
  } = props;

  const [isHovering, setIsHovering] = useState(false);
  const numberWithCommas = (x) => {
    return x && x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
  };
  useEffect(() => {
    if(vat) changeVatValue(vat)
  })
  return (
    <div
      className={style.containerElement}
      style={{ display: 'block' }}
      onMouseEnter={() => setIsHovering(true)}
      onMouseLeave={() => setIsHovering(false)}
    >
      <h3>{_l`Scope of work`}</h3>
      <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
        {/* <h5>{_l`Services / Products`}</h5> */}
        {__MODE === 'BUILD' && <SettingProductColumn />}
      </div>

      <Table celled>
        <Table.Header>
          {/* <Table.Row>
            <Table.HeaderCell textAlign="center" colSpan={3}>{_l`Product`}</Table.HeaderCell>
            <Table.HeaderCell textAlign="center" colSpan={3}>{_l`Units`}</Table.HeaderCell>
            <Table.HeaderCell textAlign="center" colSpan={3}>{_l`Discount`}</Table.HeaderCell>
            <Table.HeaderCell colSpan={4}></Table.HeaderCell>
          </Table.Row> */}
          <Table.Row>
            {__SETTING_COLUMN_PRODUCT?.productGroup && <Table.HeaderCell>{_l`Product group`}</Table.HeaderCell>}
            {__SETTING_COLUMN_PRODUCT?.product && <Table.HeaderCell>{_l`Product`}</Table.HeaderCell>}
            {__SETTING_COLUMN_PRODUCT?.productType && <Table.HeaderCell>{_l`Product type`}</Table.HeaderCell>}
            {__SETTING_COLUMN_PRODUCT?.startDate && __REVENUETYPE !== REVENUETYPE.FIXED_RECURRING && (
              <Table.HeaderCell>{_l`Start date`}</Table.HeaderCell>
            )}
            {__SETTING_COLUMN_PRODUCT?.endDate && __REVENUETYPE !== REVENUETYPE.FIXED_RECURRING && (
              <Table.HeaderCell>{_l`End date`}</Table.HeaderCell>
            )}

            {__SETTING_COLUMN_PRODUCT?.unitAmount && (
              <Table.HeaderCell collapsing textAlign="center">{_l`Unit amount`}</Table.HeaderCell>
            )}
            {__SETTING_COLUMN_PRODUCT?.unitPrice && (
              <Table.HeaderCell collapsing textAlign="center">{_l`Price per unit`}</Table.HeaderCell>
            )}
            {__SETTING_COLUMN_PRODUCT?.unitCost && <Table.HeaderCell collapsing>{_l`Unit cost`}</Table.HeaderCell>}
            {__SETTING_COLUMN_PRODUCT?.discountPercent && (
              <Table.HeaderCell collapsing>{_l`Discount %`}</Table.HeaderCell>
            )}
            {__SETTING_COLUMN_PRODUCT?.discountPrice && (
              <Table.HeaderCell collapsing>{_l`Discounted price`}</Table.HeaderCell>
            )}
            {__SETTING_COLUMN_PRODUCT?.discountAmount && (
              <Table.HeaderCell collapsing>{_l`Discount amount`}</Table.HeaderCell>
            )}
            {__SETTING_COLUMN_PRODUCT?.margin && (
              <Table.HeaderCell collapsing textAlign="center">{_l`Margin %`}</Table.HeaderCell>
            )}
            {__SETTING_COLUMN_PRODUCT?.cost && <Table.HeaderCell textAlign="center">{_l`Cost`}</Table.HeaderCell>}
            {__SETTING_COLUMN_PRODUCT?.occupied && <Table.HeaderCell collapsing>{_l`Occupied`}</Table.HeaderCell>}
            {__SETTING_COLUMN_PRODUCT?.profit && <Table.HeaderCell collapsing>{_l`Profit`}</Table.HeaderCell>}
            {__SETTING_COLUMN_PRODUCT?.description && <Table.HeaderCell collapsing>{_l`Description`}</Table.HeaderCell>}
            {__SETTING_COLUMN_PRODUCT?.value && <Table.HeaderCell textAlign="center">{_l`Value`}</Table.HeaderCell>}
          </Table.Row>
        </Table.Header>
        <Table.Body>
          {__PRODUCT.map((e, index) => {
            // let cost = (e.costUnit || 0) * (e.numberOfUnit || 0);
            // let value =
            //   e.type === 'RECURRING' && e.periodNumber
            //     ? e.numberOfUnit * e.price * e.periodNumber
            //     : e.numberOfUnit * e.price;
            // let profit = e.numberOfUnit * e.price - (e.costUnit || 0) * (e.numberOfUnit || 0);
            return (
              <Table.Row key={index}>
                {__SETTING_COLUMN_PRODUCT?.productGroup && <Table.Cell>{e.lineOfBusinessName}</Table.Cell>}
                {__SETTING_COLUMN_PRODUCT?.product && <Table.Cell>{e.productDTO?.name}</Table.Cell>}
                {__SETTING_COLUMN_PRODUCT?.productType && <Table.Cell>{e.measurementTypeName}</Table.Cell>}
                {__SETTING_COLUMN_PRODUCT?.startDate && __REVENUETYPE !== REVENUETYPE.FIXED_RECURRING && (
                  <Table.Cell>
                    {e.deliveryStartDate ? moment(e.deliveryStartDate).format('DD MMM, YYYY') : ''}
                  </Table.Cell>
                )}
                {__SETTING_COLUMN_PRODUCT?.endDate && __REVENUETYPE !== REVENUETYPE.FIXED_RECURRING && (
                  <Table.Cell>{e.deliveryEndDate ? moment(e.deliveryEndDate).format('DD MMM, YYYY') : ''}</Table.Cell>
                )}

                {__SETTING_COLUMN_PRODUCT?.unitAmount && <Table.Cell textAlign="center">{e.numberOfUnit}</Table.Cell>}
                {__SETTING_COLUMN_PRODUCT?.unitPrice && <Table.Cell textAlign="right">{e.price}</Table.Cell>}
                {__SETTING_COLUMN_PRODUCT?.unitCost && <Table.Cell textAlign="right">{e.costUnit}</Table.Cell>}
                {__SETTING_COLUMN_PRODUCT?.discountPercent && (
                  <Table.Cell textAlign="right">{e.discountPercent}</Table.Cell>
                )}
                {__SETTING_COLUMN_PRODUCT?.discountPrice && (
                  <Table.Cell textAlign="right">{e.discountedPrice}</Table.Cell>
                )}
                {__SETTING_COLUMN_PRODUCT?.discountAmount && (
                  <Table.Cell textAlign="right">{e.discountAmount}</Table.Cell>
                )}
                {__SETTING_COLUMN_PRODUCT?.margin && <Table.Cell textAlign="right">{e.margin}</Table.Cell>}
                {__SETTING_COLUMN_PRODUCT?.cost && <Table.Cell textAlign="right">{e.cost}</Table.Cell>}
                {__SETTING_COLUMN_PRODUCT?.occupied && (
                  <Table.Cell textAlign="right">{e.occupied && numberWithCommas(e.occupied)}</Table.Cell>
                )}
                {__SETTING_COLUMN_PRODUCT?.profit && <Table.Cell textAlign="right">{e.profit}</Table.Cell>}
                {__SETTING_COLUMN_PRODUCT?.description && <Table.Cell textAlign="left">{e.description}</Table.Cell>}
                {__SETTING_COLUMN_PRODUCT?.value && <Table.Cell textAlign="right">{e.value}</Table.Cell>}
              </Table.Row>
            );
          })}
        </Table.Body>
      </Table>
      <Grid>
        <GridRow>
          <GridColumn width={8}></GridColumn>
          <GridColumn width={8}>
            <h5>{_l`Price summary`}</h5>
            <div>
              <div className={style.dflex}>
                <p>{_l`NetESign`}</p>
                <p>
                  {__TOTAL_VALUE_PRODUCT?.net || 0} {currency}
                </p>
              </div>
              <div className={style.dflex}>
                <p>{_l`VAT`} (%)</p>
                {__MODE === 'BUILD' ? (
                  <Input
                    // value={vat}
                    value={ vat || __TOTAL_VALUE_PRODUCT?.vat || 0}
                    onChange={(e) => changeVatValue(e.target.value)}
                    type="number"
                    style={{ width: 70 }}
                  />
                ) : (
                  <p>{__TOTAL_VALUE_PRODUCT?.vat || 0}</p>
                )}
              </div>
              <Divider />
              <div className={style.dflex}>
                <p>{_l`Total`}</p>
                <p>
                  {__TOTAL_VALUE_PRODUCT?.total || 0} {currency}
                </p>
              </div>
            </div>
          </GridColumn>
        </GridRow>
      </Grid>

      {/* <Table>
        <Table.Body>
          <Table.Row>
            <Table.Cell width={10}>Net</Table.Cell>
            <Table.Cell textAlign="right"></Table.Cell>
          </Table.Row>
          <Table.Row>
            <Table.Cell width={10}>VAT (%)</Table.Cell>
            <Table.Cell textAlign="right">
              {__MODE === 'BUILD' ? (
                <Input value={vat} onChange={(e) => setVat(e.target.value)} type="number" style={{ width: 70 }} />
              ) : (
                `${vat}`
              )}
            </Table.Cell>
          </Table.Row>
        </Table.Body>
        <Table.Footer>
          <Table.Row>
            <Table.HeaderCell width={10}>Total</Table.HeaderCell>
            <Table.HeaderCell textAlign="right">{`${__TOTAL_VALUE_PRODUCT?.total || 0} ${currency}`}</Table.HeaderCell>
          </Table.Row>
        </Table.Footer>
      </Table> */}
      {isHovering && __MODE === 'BUILD' && (
        <div className={style.GroupControllerButton}>
          <GroupControllerButton index={props.index} type={BLOCKTYPE_SIGNATURE.PRICING} />
        </div>
      )}
    </div>
  );
};

const mapStateToProps = (state) => ({
  __PRODUCT: state?.entities?.signature?.__PRODUCT,
  __SETTING_COLUMN_PRODUCT: state?.entities?.signature?.__SETTING_COLUMN_PRODUCT,
  __MODE: state?.entities?.signature?.__MODE,
  currency: state?.ui?.app?.currency,
  __TOTAL_VALUE_PRODUCT: state?.entities?.signature?.__TOTAL_VALUE_PRODUCT,
  __REVENUETYPE: state?.entities?.signature?.__REVENUETYPE,
  vat: state?.entities?.signature?.__ELEMENTS?.find((e) => e?.type === BLOCKTYPE_SIGNATURE.PRICING)?.vat
});

const mapDispatchToProps = {
  changeVatValue,
};

export default connect(mapStateToProps, mapDispatchToProps)(PricingService);
