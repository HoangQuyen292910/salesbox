import React, { useState, useRef, useEffect } from 'react';
import { connect } from 'react-redux';
import { Button, Icon } from 'semantic-ui-react';
import style from '../Signature.css';
import _l from 'lib/i18n';
import cx from 'classnames';
import GroupControllerButton from './GroupControllerButton';
import { BLOCKTYPE_SIGNATURE, Endpoints } from '../../../Constants';
import { uploadAttachment, uploadUrlAttachment } from '../signature.actions';
import * as NotificationActions from '../../Notification/notification.actions';
import pdfIcon from '../../../../public/pdfNew.svg';
import docxIcon from '../../../../public/google-docs-color.svg'
import sheetIcon from '../../../../public/google-sheets-color.svg'
import presentationIcon from '../../../../public/google-slides-color.svg'
import api from 'lib/apiClient';

export const Attachment = (props) => {
  const { __ELEMENT_DATA, uploadAttachment, notiError, __MODE, uploadUrlAttachment } = props;
  const [isHovering, setIsHovering] = useState(false);
  const refFile = useRef(null);
  const [listFiles, setListFiles] = useState([])
  const handleChangeFile = (e) => {
    let files = [...__ELEMENT_DATA?.attachments];
    if (e.target.files) {
      Object.keys(e.target.files).forEach((key) => {
        console.log(e.target.files[key]);
        if (e.target.files[key].size > 20971520) {
          // Max file size is 20MB
          notiError(`Maximum file size is 20MB`);
          return;
        }
        files.push(e.target.files[key]);
      });
    }
    uploadAttachment(files);
    uploadAttachmentToSave(files);
  };

  const uploadAttachmentToSave = async (files) => {
    try {
    let formData = new FormData();
    files?.forEach((file) => {
      // __ELEMENT_DATA?.attachments?.forEach((file) => {
        formData.append('attachments', file);
      });
      const res = await api.post({
        resource: `${Endpoints.Enterprise}/e-signature/uploadAttachments`,
          data: formData
      })
      uploadUrlAttachment(res.attachmentsUrl)
    } catch (error) {
      console.log(error)
    }
  }

  const convertUrlToFile = async (urlFiles) => {
    // let urlFiles = attachmentsBlock?.attachmentsUrl
    let array = []
    let indexFile = []
    await Promise?.all(urlFiles?.map(async (url, indexKey) => {
      let response = await fetch(url);
      let data = await response.blob();
      console.log("convert to url", data)
      array.push(new File([data], url.match(/\/([^\/?#]+)[^\/]*$/)[1].split(/\_(?=[^\.]+$)/)[0],  { type: data.type }))
      indexFile.push(indexKey)
    }))
    uploadAttachment(indexFile.map(i => array[i]));
  }
  useEffect(()=>{
    if(__ELEMENT_DATA?.attachments.length === 0 && __ELEMENT_DATA?.attachmentsUrl?.length !== 0){
      convertUrlToFile(__ELEMENT_DATA?.attachmentsUrl)
    }
  })
  const removeFile = (index) => {
    if (__ELEMENT_DATA?.attachments) {
      let files = [
        ...__ELEMENT_DATA.attachments.slice(0, index),
        ...__ELEMENT_DATA.attachments.slice(index + 1, __ELEMENT_DATA.attachments.length),
      ];
      let imageList = [
        ...listFiles.slice(0, index),
        ...listFiles.slice(index+1, listFiles.length)
      ]
      setListFiles(imageList)
      uploadAttachment(files);
    }
    if(__ELEMENT_DATA?.attachmentsUrl){
      let files = [
        ...__ELEMENT_DATA.attachmentsUrl.slice(0, index),
        ...__ELEMENT_DATA.attachmentsUrl.slice(index + 1, __ELEMENT_DATA.attachmentsUrl.length),
      ];
      uploadUrlAttachment(files);
    }
  };

  const supportTypeIcon = ["pdf", "image", "wordprocessing", "sheet", "presentation"]
  return (
    <div
      className={style.containerElement}
      onMouseEnter={() => setIsHovering(true)}
      onMouseLeave={() => setIsHovering(false)}
    >
      <h3>{_l`Attachments`}</h3>
      {__ELEMENT_DATA?.attachments?.map((e, index) => {
        let type = e.type
        let reader = new FileReader();

        if(type?.includes("image") && !type?.includes('svg')){
          reader.readAsDataURL(e)
          reader.onload = function () {
            let obj = {
              base64: reader.result
            }
            let newData = listFiles
            if(_.size(listFiles) < __ELEMENT_DATA?.attachments.length ){
              let i = parseInt(index)
              newData[i]=reader.result;
              setListFiles(newData)
            }
          }
        }
        return (
          <div className={style.fileItem} key={index}>
            { !type?.includes('svg') && supportTypeIcon.some(check => type?.includes(check)) ?
            <div style={{ display: 'flex', justifyContent: 'flex-start', alignItems: 'center' }}>
              { type?.includes("pdf")  && <img key={index} style={{width: 25}} src={pdfIcon} />}
              { type?.includes("wordprocessing") && <img key={index} style={{width: 25}} src={docxIcon} /> }
              { type?.includes("image") &&  <img src={listFiles[index]} key={index} style={{width: 24}}/> }
              { type?.includes("sheet") && <img key={index} style={{width: 25}} src={sheetIcon} />}
              { type?.includes("presentation") && <img key={index} style={{width: 25}} src={presentationIcon} />}
              <div style={{padding: 10}}>{e.name}</div>
            </div>
            :
              <div style={{ display: 'flex', justifyContent: 'flex-start', alignItems: 'center' }}>
                <Icon name="attach" size="large" style={{fontSize: 20 }} />
              <div style={{padding: 10}}>{e.name}</div> </div>
            }
            {__MODE === 'BUILD' && (
              <Icon name="remove" style={{ color: 'red', cursor: 'pointer' }} onClick={() => removeFile(index)} />
            )}
          </div>
        );
      })}
      <input ref={refFile} type="file" multiple hidden onChange={handleChangeFile} />
      {__MODE === 'BUILD' && (
        <div style={{ display: 'flex', justifyContent: 'center' }}>
          <Button size="medium" onClick={() => refFile.current.click()}>
            <Icon name="cloud upload" />
            {_l`Upload file`}
          </Button>
        </div>
      )}

      {isHovering && props.__MODE === 'BUILD' && (
        <div className={style.GroupControllerButton}>
          <GroupControllerButton type={BLOCKTYPE_SIGNATURE.ATTACHMENT} index={props.index} />
        </div>
      )}
    </div>
  );
};

const mapStateToProps = (state) => ({
  __MODE: state?.entities?.signature?.__MODE,
  __ELEMENT_DATA: state?.entities?.signature?.__ELEMENTS?.find((e) => e.type === BLOCKTYPE_SIGNATURE.ATTACHMENT),
});

const mapDispatchToProps = {
  uploadAttachment,
  notiError: NotificationActions.error,
  uploadUrlAttachment
};

export default connect(mapStateToProps, mapDispatchToProps)(Attachment);
