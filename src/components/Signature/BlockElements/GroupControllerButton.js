import React from 'react';
import { connect } from 'react-redux';
import { Button, ButtonGroup, Icon } from 'semantic-ui-react';
import style from '../Signature.css';
import _l from 'lib/i18n';
import {
  removeBlock,
  reorderElement,
  setVisibleRightMenu,
  removeElementInColumnBlock,
  reorderElementInColumnBlock,
} from '../signature.actions';

export const GroupControllerButton = (props) => {
  const {
    hasEdit,
    type,
    setVisibleRightMenu,
    index,
    inColumnBlock,
    parentColumnIndex,
    parentBlockIndex,
    removeElementInColumnBlock,
    reorderElementInColumnBlock,
  } = props;

  const removeBlock = () => {
    if (inColumnBlock) {
      removeElementInColumnBlock(parentBlockIndex, parentColumnIndex, index);
    } else {
      props.removeBlock(props.index);
    }
  };
  const reorderElement = (oldIndex, newIndex) => {
    if (inColumnBlock) {
      reorderElementInColumnBlock(parentBlockIndex, parentColumnIndex, oldIndex, parentColumnIndex, newIndex);
    } else {
      props.reorderElement(oldIndex, newIndex);
    }
  };
  return (
    <div className={style.dflex}>
      <div>
        {hasEdit && !inColumnBlock && (
          <Button
            size="mini"
            icon={<Icon name="setting" />}
            content={_l`Edit`}
            labelPosition="right"
            onClick={() => setVisibleRightMenu(true, type, index)}
          ></Button>
        )}
      </div>
      <div style={{ width: 40 }}>
        <ButtonGroup vertical size="mini">
          <Button
            size="mini"
            icon={<Icon name="arrow up" />}
            onClick={() => reorderElement(props.index, props.index - 1)}
          ></Button>
          <Button
            size="mini"
            icon={<Icon name="arrow down" />}
            onClick={() => reorderElement(props.index, props.index + 1)}
          ></Button>
        </ButtonGroup>
        <Button size="mini" icon={<Icon name="remove" />} style={{ marginTop: 2 }} onClick={removeBlock}></Button>
      </div>
    </div>
  );
};

const mapStateToProps = (state) => ({});

const mapDispatchToProps = {
  removeBlock,
  reorderElement,
  setVisibleRightMenu,
  removeElementInColumnBlock,
  reorderElementInColumnBlock,
};

export default connect(mapStateToProps, mapDispatchToProps)(GroupControllerButton);
