import React, { useState, useRef, useEffect } from 'react';
import { connect } from 'react-redux';
import { Button, Icon, Popup } from 'semantic-ui-react';
import _l from 'lib/i18n';
import style from '../Signature.css';
import cx from 'classnames';
import styled from 'styled-components';
import GroupControllerButton from './GroupControllerButton';
import { BLOCKTYPE_SIGNATURE } from '../../../Constants';
import ReactQuill from 'react-quill';
import 'react-quill/dist/quill.bubble.css';
import { editHeader, setPositionBlockLastestFocused, changeValueShowCropPhotoModal, setEditContent, setDataFillToEditor, convertOpsContent } from '../signature.actions';
import eSignBgDefault from '../../../../public/eSignBgDefaut.jpg';
import ModalCropPhoto from '../ModalCropPhoto';
import { ResizeProvider, ResizeConsumer } from 'react-resize-context';
import { userTags, dealTags, contactTags, companyTags, specialTags, tagMessages, ownerName, dayInPipe, dateDeal, companyCf, contactCf, dealCf } from './tagMessages';
import moment from 'moment'

export const Header = (props) => {
  const modules = {
    toolbar: [
      ['bold', 'italic', 'blockquote', { color: [] }, { background: [] }, { header: [1, 2, 3, false] }, { align: [] }],
    ],
  };
  const {
    index,
    __DEAL,
    __ELEMENT_DATA,
    __PARTER_BLOCK_DATA,
    editHeader,
    __MODE,
    valueTobeFilledToEditor,
    positionBlockLastestFocused,
    setPositionBlockLastestFocused,
    setDisableDraggable,
    __COMPANY,
    __CONTACT,
    __USER_LOGIN_INFO, dataCFCompany, dataCFContact, dataCFDeal,
    convertOpsContent,
    setDataFillToEditor,
    positionParentBlockLastestFocused
  } = props;
  const [isHovering, setIsHovering] = useState(false);
  const [selectedImage, setSelectedImage] = useState(null);
  const refQuillHeader = useRef(null);
  let quill, _nodeImage;
  const onFocusEditor = (range, source, editor) => {
    // console.log(range, source, editor);
    setPositionBlockLastestFocused(index);
    setMakeMargin(true);
  };

  const onBlueEditor = (previousRange, source, editor) => {
    setMakeMargin(false);
    // console.log('Onblue header:', previousRange, source, editor);
  };

  const [visibleCropPhotoModal, setVisibleCropPhotoModal] = useState(false);
  const LAYOUTS = {
    Layout1: 'layout1',
    Layout2: 'layout2',
    Layout3: 'layout3',
    Layout4: 'layout4',
    Layout5: 'layout5',
    Layout6: 'layout6'
  }
  useEffect(() => {
    console.log(valueTobeFilledToEditor);
    if (valueTobeFilledToEditor && index === positionBlockLastestFocused && !positionParentBlockLastestFocused) {
      quill = refQuillHeader.current.getEditor();
      quill.focus();
      let range = quill.getSelection();
      let position = range ? range.index : 0;
      quill.insertText(position, valueTobeFilledToEditor);
    }
  }, [valueTobeFilledToEditor]);

  useEffect(() => {
    console.log("🚀 ~ file: Header.js ~ line 70 ~ Header ~ __ELEMENT_DATA?.layout", __ELEMENT_DATA?.layout)
    switch (__ELEMENT_DATA?.layout) {
      case LAYOUTS.Layout1:
      case LAYOUTS.Layout2:
        editHeader('imageMainCoverSize', {
          height: __ELEMENT_DATA?.imageMainCoverSize?.height || 400,
          width: __ELEMENT_DATA?.imageMainCoverSize?.width
        });
        break;
      default:
        editHeader('imageMainCoverSize', {
          height: __ELEMENT_DATA?.imageMainCoverSize?.height || 200,
          width: __ELEMENT_DATA?.imageMainCoverSize?.width || '100%'
        });

    }
    _nodeImage = document.getElementById('resize-image-id');
    // editHeader('imageMainCoverSize', {
    //   height: 400
    // });
  }, [__ELEMENT_DATA?.layout])

  const onChangeContent = (content, delta, source, editor) => {
    editHeader('content', content, editor.getContents());
    editHeader('contentTemp', content);
    setDataFillToEditor('')
  };

  const resizeImage = (size) => {
    // let imgContainer = document.getElementsByClassName('Signature_containerLogoResiable-53') || document.getElementsByClassName(cx(style[`img${__ELEMENT_DATA?.layout}`]))
    if (!_nodeImage) {
      _nodeImage = document.getElementById('resize-image-id');
    }
    console.log("🚀 ~ file: Header.js ~ line 80 ~ resizeImage ~ _nodeImage", _nodeImage.clientWidth, _nodeImage.clientHeight)

    // let overContainer = document.getElementsByClassName('mainHeaderBlock')[0]
    // let newSize = {
    //   width: imgContainer[0]?.clientWidth,
    //   height: imgContainer[0]?.clientHeight
    // }
    // let sizeWithOther = {
    //     width: `${imgContainer[0]?.clientWidth*100 / overContainer.clientWidth}%`,
    //     height: imgContainer[0]?.clientHeight>=400 ? imgContainer[0]?.clientHeight : `${imgContainer[0]?.clientHeight*100 / overContainer?.clientHeight}%`
    // }
    // editHeader('imageMainCoverSize', startResize && imgContainer ? newSize : size );
    editHeader('imageMainCoverSize', {
      width: _nodeImage.clientWidth,
      height: _nodeImage.clientHeight
    });
    // editHeader('imageWithBlockSize', startResize && imgContainer ? sizeWithOther : size )
  };
  const onMouseDownHeader = () => {
    console.log('Disbale startt');
    setDisableDraggable(true);
  };
  const onMouseUpHeader = () => {
    console.log('Disbale stop');
    setDisableDraggable(false);
  };

  const convertToValue = ( text ) => {
    let matches = text.match(/\{{(.*?)\}}/g)
    let changeContent = text
    matches?.forEach(i => {
      if (companyTags.some(t => i.slice(0, i.indexOf("_") + 1).includes(t))) {
        let checkIndex = i.slice(i.indexOf("_") + 1, i.length - 2)
        if (specialTags[checkIndex]) {
          let key = __COMPANY?.[specialTags[checkIndex]]?.name
          changeContent = changeContent.replaceAll(i, key ? key : '')
        } else {
          let key = __COMPANY?.[tagMessages[checkIndex]]
          changeContent = changeContent.replaceAll(i, key ? key : '')
        }
      }
      if (contactTags.some(t => i.slice(0, i.indexOf("_") + 1).includes(t))) {
        let checkIndex = i.slice(i.indexOf("_") + 1, i.length - 2)
        if (specialTags[checkIndex]) {
          let key = __CONTACT?.[specialTags[checkIndex]]?.name
          changeContent = changeContent.replaceAll(i, key ? key : '')
        } else {
          let key = __CONTACT?.[tagMessages[checkIndex]]
          changeContent = changeContent.replaceAll(i, key ? key : '')
        }
      }
      if (dealTags.some(t => i.slice(0, i.indexOf("_") + 1).includes(t))) {
        let checkIndex = i.slice(i.indexOf("_") + 1, i.length - 2)
        if (Object.keys(ownerName).some(t => checkIndex.includes(t))) {
          let key = __DEAL?.participantList?.[0][ownerName[checkIndex]]?.toString()
          changeContent = changeContent.replaceAll(i, key ? key : '')
        } else {
          // let key = __DEAL?.[checkIndex]
          if (Object.keys(dayInPipe).some(t => checkIndex.includes(t))) {
            changeContent = changeContent.replaceAll(i, __DEAL[dayInPipe[checkIndex]] ? Math.ceil(__DEAL[dayInPipe[checkIndex]] / 1000 / 3600 / 24)?.toString() : '')
          }
          if (Object.keys(dateDeal).some(t => checkIndex.includes(t))) {
            changeContent = changeContent.replaceAll(i, __DEAL[dateDeal[checkIndex]] ? new moment(__DEAL[dateDeal[checkIndex]]).format('DD-MM-YYYY')?.toString() : '')
          } else {
            changeContent = changeContent.replaceAll(i, __DEAL[tagMessages[checkIndex]] ? __DEAL[tagMessages[checkIndex]] : '')
          }
        }
      }
      if (userTags.some(t => i.slice(0, i.indexOf("_") + 1).includes(t))) {
        let checkIndex = i.slice(i.indexOf("_") + 1, i.length - 2)
        let key = __USER_LOGIN_INFO?.[tagMessages[checkIndex]]
        changeContent = changeContent.replaceAll(i, key ? key : '')
      }
      if (companyCf.some(t => i.slice(0, i.indexOf("_") + 1).includes(t))) {
        let thisUuid = i.slice(i.indexOf("_") + 1, i.length - 2)
        let value = dataCFCompany?.find((element) => element.title === thisUuid);
        let key = value.customFieldValueDTOList[0]?.value
        if (value?.customFieldValueDTOList?.[0]?.value) {
          changeContent = changeContent.replace(i, key)
        } else {
          changeContent = changeContent.replaceAll(i, '')
        }
      }
      if (contactCf.some(t => i.slice(0, i.indexOf("_") + 1).includes(t))) {
        let thisUuid = i.slice(i.indexOf("_") + 1, i.length - 2)
        let value = dataCFContact?.find((element) => element.title === thisUuid);
        let key = value.customFieldValueDTOList[0]?.value?.toString()
        changeContent = changeContent.replaceAll(i, key ? key : '')
      }
      if (dealCf.some(t => i.slice(0, i.indexOf("_") + 1).includes(t))) {
        let thisUuid = i.slice(i.indexOf("_") + 1, i.length - 2)
        let value = dataCFDeal?.find((element) => element.title === thisUuid);
        let key = value.customFieldValueDTOList[0]?.value
        changeContent = changeContent.replaceAll(i, key ? key : '')
      }
    })
    return changeContent
  }

  useEffect(() => {
    let arrOps = __ELEMENT_DATA?.contentPdf?.ops
    arrOps?.forEach(i => {
      i.insert = convertToValue(i.insert)
    })
    convertOpsContent(index, 'contentPdf', {ops: arrOps})
  }, [__ELEMENT_DATA?.contentPdf?.ops])

  // useEffect(() => {
  //   if (__MODE === "PREVIEW") {
  //     let newContent = convertToValue(__ELEMENT_DATA?.content)
  //     const quill = refQuillHeader.current.getEditor();
  //     quill.focus();
  //     // setEditContent(changeContent, index, quill.getContents())
  //     editHeader('contentTemp', newContent)
  //   }
  // }, [__MODE, __ELEMENT_DATA?.contentTemp, __ELEMENT_DATA?.content])
  const [makeMargin, setMakeMargin] = useState(false);
  return (
    <div
      id="divMainCoverESignature"
      style={{
        minHeight: 400,
        padding: __ELEMENT_DATA?.layout === LAYOUTS.Layout1 || __ELEMENT_DATA?.layout === LAYOUTS.Layout2 ? 0 : '0 80',
        marginBottom: makeMargin && __MODE !== 'PREVIEW' ? 140 : 0  }}
      className={cx(style.containerElement, style[`div${__ELEMENT_DATA?.layout}`], `mainHeaderBlock`)}
      onMouseEnter={() => setIsHovering(true)}
      onMouseLeave={() => setIsHovering(false)}
    >
      {__ELEMENT_DATA?.backgroundFilter ? (
        <div
          className={cx(style[`img${__ELEMENT_DATA?.layout}`])}
          style={{ background: __ELEMENT_DATA?.backgroundFilter }}
        ></div>
      ) :
        (__MODE === 'PREVIEW' ?
          <div>
            <img
              style={{
                width: __ELEMENT_DATA?.imageMainCoverSize?.width || '100%',
                height: __ELEMENT_DATA?.imageMainCoverSize?.height || '100%',
              }}
              src={__ELEMENT_DATA?.background ? __ELEMENT_DATA?.background : eSignBgDefault}
              className={cx(style[`img${__ELEMENT_DATA?.layout}`])}
            />
          </div>
          :
          <ResizeProvider>
            <ResizeConsumer style={{ maxWidth: __ELEMENT_DATA?.layout === 'layout1' || __ELEMENT_DATA?.layout === 'layout2' ? '60%' : '100%' }} onSizeChanged={resizeImage} updateDatasetBySize={() => { }}>
              <div
                className={style.containerLogoResiable}
                onMouseEnter={onMouseDownHeader}
                onMouseLeave={onMouseUpHeader}
                id="resize-image-id"
              // style={{ width: __MODE === 'PREVIEW' ? `${(__ELEMENT_DATA?.imageMainCoverSize?.width / widthA4) * 100}% !important` : 'auto' }}
              >
                <Popup size="tiny" content={_l`width` + `: ${__ELEMENT_DATA?.imageMainCoverSize?.width}px, ` + _l`height` + `: ${__ELEMENT_DATA?.imageMainCoverSize?.height}px`}
                  trigger={
                    <img
                      style={{
                        width: __ELEMENT_DATA?.imageMainCoverSize?.width || '100%',
                        height: __ELEMENT_DATA?.imageMainCoverSize?.height || '100%',
                      }}
                      src={__ELEMENT_DATA?.background ? __ELEMENT_DATA?.background : eSignBgDefault}
                      className={cx(style[`img${__ELEMENT_DATA?.layout}`])}
                    />} />
              </div>
            </ResizeConsumer>
          </ResizeProvider>
        )

      }

      <div
      onMouseMove={() => {
        if (!_nodeImage) {
          _nodeImage = document.getElementById('resize-image-id');
        }
        if(_nodeImage?.clientWidth >= 476 && __ELEMENT_DATA?.layout === LAYOUTS.Layout1 && _nodeImage?.clientHeight !== __ELEMENT_DATA?.imageMainCoverSize?.height && __MODE !== 'PREVIEW') resizeImage()
      }}
        style={{
          padding: __ELEMENT_DATA?.layout === LAYOUTS.Layout1 || __ELEMENT_DATA?.layout === LAYOUTS.Layout2 ? '30 50' : '0 0 30',
          display: 'flex',
          width: '100%',
          flexDirection: 'column',
          justifyContent: 'space-between',
          background: __ELEMENT_DATA?.backgroundColor,
          textAlign: __ELEMENT_DATA?.textAlign,
        }}
      >
        <div
          style={{ minHeight: 29 }}
          onMouseDown={onMouseDownHeader}
          onMouseEnter={onMouseDownHeader}
          onMouseLeave={onMouseUpHeader}
        />
        <div>
          <ReactQuill
            modules={modules}
            onFocus={onFocusEditor}
            onBlur={onBlueEditor}
            readOnly={__MODE === 'PREVIEW'}
            ref={refQuillHeader}
            id="ReactQuillBubble"
            // value={__ELEMENT_DATA?.content}
            onChange={onChangeContent}
            onChangeSelection={(range, source, editor) => {
              if (range && range.length !== 0) setMakeMargin(true);
              else setMakeMargin(false);
            }}
            onBlur={() => setMakeMargin(false)}
            // style={{ height: 'fit-content', marginBottom: makeMargin && __MODE !== 'PREVIEW' ? 70 : 0 }}
            value={__MODE === 'PREVIEW' ? __ELEMENT_DATA?.contentTemp : __ELEMENT_DATA?.content || ''}
            className={style.reactQuill}
            theme="bubble"
          />
          {/* <input
            className={cx(style.inputTextElement)}
            style={{
              fontSize: 30,
              background: 'transparent',
              color: __ELEMENT_DATA?.textColor,
              textAlign: __ELEMENT_DATA?.textAlign,
            }}
            placeholder={_l`Proposal`}
            value={value1}
            onChange={onChangeTitle}
          ></input>
          <span
            id="DESCRIPTION-HEADER"
            role="textbox"
            contentEditable
            className={style.textarea}
            style={{ color: __ELEMENT_DATA?.textColor }}
          ></span> */}
        </div>
        {/* <div style={{ color: __ELEMENT_DATA?.textColor }}>
          <p>{_l`Prepared for`}{` `} {`${(__PARTER_BLOCK_DATA?.__COMPANY_CONTACT_INFO?.[0]?.companyName?.value !== '' &&
            __PARTER_BLOCK_DATA?.__COMPANY_CONTACT_INFO?.[0]?.companyName?.value) ||
            __PARTER_BLOCK_DATA?.__COMPANY_CONTACT_INFO?.[0]?.fullName?.value ||
            ''}`}</p>
          <p>{_l`By`}{` `}{`${__PARTER_BLOCK_DATA?.__USER_INFO?.[0]?.fullName?.value || ''}`}</p>
        </div> */}
      </div>
      {isHovering && __MODE === 'BUILD' && (
        <div className={style.GroupControllerButton}>
          <GroupControllerButton type={BLOCKTYPE_SIGNATURE.HEADER} index={props.index} hasEdit={true} />
        </div>
      )}

      <ModalCropPhoto
        visible={visibleCropPhotoModal}
        onClose={() => {
          setVisibleCropPhotoModal(false);
          changeValueShowCropPhotoModal(false);
        }}
        onDone={(result) => {
          editHeader('logo', result);
          setVisibleCropPhotoModal(false);
          changeValueShowCropPhotoModal(false);
        }}
        rootImg={selectedImage}
      />
    </div>
  );
};

const mapStateToProps = (state) => ({
  __DEAL: state.entities?.signature?.__DEAL,
  __ELEMENT_DATA: state?.entities?.signature?.__ELEMENTS?.find((e) => e?.type === BLOCKTYPE_SIGNATURE.HEADER),
  __PARTER_BLOCK_DATA: state?.entities?.signature?.__ELEMENTS?.find((e) => e?.type === BLOCKTYPE_SIGNATURE.PARTER),
  __MODE: state?.entities?.signature?.__MODE,
  valueTobeFilledToEditor: state?.entities?.signature?.valueTobeFilledToEditor,
  positionBlockLastestFocused: state?.entities?.signature?.positionBlockLastestFocused,
  positionParentBlockLastestFocused: state?.entities?.signature?.positionParentBlockLastestFocused,
  __COMPANY: state?.entities?.signature?.__COMPANY,
  __CONTACT: state?.entities?.signature?.__CONTACT,
  __USER_LOGIN_INFO: state?.entities?.signature?.__USER_LOGIN_INFO,
});

const mapDispatchToProps = {
  editHeader,
  setPositionBlockLastestFocused,
  changeValueShowCropPhotoModal,
  setEditContent,
  convertOpsContent,
  setDataFillToEditor
};

export default connect(mapStateToProps, mapDispatchToProps)(Header);
