import React, { useState } from 'react';
import { connect } from 'react-redux';
import _l from 'lib/i18n';
import style from '../Signature.css';
import { Table, Input, Icon, Button, Grid, GridColumn, GridRow } from 'semantic-ui-react';
import GroupControllerButton from './GroupControllerButton';
import { BLOCKTYPE_SIGNATURE, Endpoints } from '../../../Constants';
import { editParterBlock, addMoreParter, deleteOneParty } from '../signature.actions';
import ModalCommon from '../../ModalCommon/ModalCommon';
import OrganisationDropdown from '../../Organisation/OrganisationDropdown';
import ContactDropdown from 'components/Contact/ContactDropdown';
import api from '../../../lib/apiClient';

export const Parties = (props) => {
  const { __DEAL, editParterBlock, __ELEMENT_DATA, __MODE, addMoreParter, isPDF, deleteOneParty } = props;

  const [isHovering, setIsHovering] = useState(false);
  const [visibleModalEdit, setVisibleModalEdit] = useState(false);

  const [editFormType, setEditFormType] = useState(null);
  const [dataEdit, setDataEdit] = useState({});

  const [isFrom, setIsFrom] = useState(true);

  // Info of 01-TO block
  const editCompanyContactInfo = (index) => {
    setEditFormType('__COMPANY_CONTACT_INFO');
    setVisibleModalEdit(true);
    const data = __ELEMENT_DATA[`__COMPANY_CONTACT_INFO`][index];
    setDataEdit({
      name: data.companyName,
      vat: data.vatNumber,
      address: data.address,
      fullName: data.fullName,
      email: data.email,
      phone: data.phone,
      title: data.title,
      firstName: data.firstName,
      lastName: data.lastName,
      country: data.country,
    });
    setIndexParterEdit(index);
  };
  const deleteUserInfo = (index) => {
    setEditFormType('__USER_INFO');
    const data = __ELEMENT_DATA[`__USER_INFO`][index];
    setDataEdit({
      name: '',
      vat: '',
      address: '',
      fullName: '',
      email: null,
      phone: null,
      title: null,
      firstName: null,
      lastName: null,
      country: null,
    });
    setIndexParterEdit(index);
    setIsFrom(false);
    deleteOneParty('__USER_INFO', index);
  };
  const deleteCompanyContactInfo = (index) => {
    setEditFormType('__COMPANY_CONTACT_INFO');
    const data = __ELEMENT_DATA[`__COMPANY_CONTACT_INFO`][index];
    setDataEdit({
      name: '',
      vat: '',
      address: '',
      fullName: '',
      email: '',
      phone: '',
      title: '',
      firstName: '',
      lastName: '',
      country: '',
    });
    setIndexParterEdit(index);
    deleteOneParty('__COMPANY_CONTACT_INFO', index);
  };
  const editUserInfo = (index) => {
    setEditFormType('__USER_INFO');
    setVisibleModalEdit(true);
    const data = __ELEMENT_DATA[`__USER_INFO`][index];
    setDataEdit({
      name: data.companyName,
      vat: data.vatNumber,
      address: data.address,
      fullName: data.fullName,
      email: data.email,
      phone: data.phone,
      title: data.title,
      firstName: data.firstName,
      lastName: data.lastName,
      country: data.country,
    });
    setIndexParterEdit(index);
  };

  const [indexParterEdit, setIndexParterEdit] = useState(0);
  const onCloseModalEdit = () => {
    setVisibleModalEdit(false);
    setIndexParterEdit(0);
  };
  const onDone = () => {
    // action here
    editParterBlock(editFormType, dataEdit, indexParterEdit);
    onCloseModalEdit();
  };

  const updateData = (key, value) => {
    if (key === 'firstName') {
      let tempLast = dataEdit['lastName'].value;
      setDataEdit({
        ...dataEdit,
        [key]: { show: dataEdit[key].show, value },
        ['fullName']: { show: dataEdit['fullName'].show, value: value + ' ' + tempLast },
      });
    } else if (key === 'lastName') {
      let tempFirst = dataEdit['firstName'].value;
      setDataEdit({
        ...dataEdit,
        [key]: { show: dataEdit[key].show, value },
        ['fullName']: { show: dataEdit['fullName'].show, value: tempFirst + ' ' + value },
      });
    } else setDataEdit({ ...dataEdit, [key]: { show: dataEdit[key].show, value } });
  };

  const [visibleModalAddParter, setVisibleModalAddParter] = useState(false);
  const [dataModalAddParter, setDataModalAddParter] = useState({
    companyId: null,
    contactId: null,
  });

  const [errorContactAddParter, setErrorContactAddParter] = useState(false);

  const addFromUser = () => {};

  const addCompanyContactInfo = () => {
    setVisibleModalAddParter(true);
  };
  const onDoneModalAddParter = async () => {
    if (!dataModalAddParter.contactId) {
      setErrorContactAddParter(true);
      return;
    }
    try {
      let resCompany;
      let resContact;
      if (dataModalAddParter.companyId) {
        resCompany = await api.get({
          resource: `${Endpoints.Organisation}/getDetails/${dataModalAddParter.companyId}`,
          query: {
            languageCode: 'en',
          },
        });
      }
      if (dataModalAddParter.contactId) {
        resContact = await api.get({
          resource: `${Endpoints.Contact}/getDetails/${dataModalAddParter.contactId}`,
          query: {
            languageCode: 'en',
          },
        });
      }

      let dataToAdd = {};
      if (resCompany) {
        dataToAdd.companyName = { value: resCompany.name, show: true };
        dataToAdd.vatNumber = { value: resCompany.vatNumber, show: true };
        dataToAdd.address = { value: resCompany.fullAddress, show: true };
        dataToAdd.country = { value: resCompany.country || null, show: true };
      }
      if (resContact) {
        dataToAdd.firstName = { show: true, value: `${resContact.firstName}` };
        dataToAdd.lastName = { show: true, value: `${resContact.lastName}` };
        dataToAdd.fullName = { show: true, value: `${resContact.firstName} ${resContact.lastName}` };
        dataToAdd.email = { show: true, value: resContact.email };
        dataToAdd.phone = { show: true, value: resContact.phone };
        dataToAdd.title = { show: true, value: resContact.title };
        dataToAdd.country = { show: true, value: resContact.country || null };
      }

      addMoreParter('__COMPANY_CONTACT_INFO', dataToAdd);
      onCloseModalAddParter();
    } catch (error) {
      console.log(error);
    }
  };
  const onCloseModalAddParter = () => {
    setVisibleModalAddParter(false);
    setErrorContactAddParter(false);
    setDataModalAddParter({});
  };

  const styleBtnEditInfo = {
    padding: 5,
    position: 'absolute',
    right: 30,
    top: 2,
  };
  const styleBtnRemoveParty = {
    padding: 5,
    position: 'absolute',
    right: 0,
    top: 2,
  }
  return (
    <div
      className={style.containerElement}
      onMouseEnter={() => setIsHovering(true)}
      onMouseLeave={() => setIsHovering(false)}
    >
      <h3>{_l`Parties`}</h3>
      <div className={style.dflex}>
        <h4 style={{ marginBottom: 0 }}>01 - {_l`To`}</h4>
        {__MODE === 'BUILD' && (
          <Button
            style={{ padding: 5 }}
            size="mini"
            icon={<img src={require('../../../../public/Add.svg')} height={12} />}
            onClick={addCompanyContactInfo}
          ></Button>
        )}
      </div>
      {__ELEMENT_DATA?.__COMPANY_CONTACT_INFO?.map((item, index) => {
        return (
          <div style={{ position: 'relative', marginTop: 5 }} key={index}>
            {__MODE === 'BUILD' && (
              <>
                <Button
                  style={styleBtnEditInfo}
                  size="mini"
                  icon={<img src={require('../../../../public/Edit.svg')} height={12} />}
                  onClick={() => editCompanyContactInfo(index)}
                ></Button>
                <Button
                  style={styleBtnRemoveParty}
                  size="mini"
                  icon={<img src={require('../../../../public/close-task.svg')} height={12} />}
                  onClick={() => deleteCompanyContactInfo(index)}
                ></Button>
              </>
            )}
            <Table celled>
              <Table.Body>
                <Table.Row verticalAlign="top">
                  <Table.Cell width={8}>
                    {_l`Company name`}
                    <h5>{item?.companyName?.value}</h5>
                  </Table.Cell>
                  <Table.Cell width={8}>
                    {_l`Contact name`}
                    <h5>{item?.fullName?.value}</h5>
                  </Table.Cell>
                </Table.Row>
                <Table.Row verticalAlign="top">
                  <Table.Cell width={8}>
                    {_l`VAT number`}
                    <h5>{item?.vatNumber?.value}</h5>
                  </Table.Cell>
                  <Table.Cell width={8}>
                    {_l`Contact email`}
                    <h5>{item?.email?.value}</h5>
                  </Table.Cell>
                </Table.Row>
                <Table.Row verticalAlign="top">
                  <Table.Cell width={8}>
                    {_l`Company address`}
                    <h5>{item?.address?.value}</h5>
                  </Table.Cell>
                  <Table.Cell width={8}>
                    {_l`Contact phone`}
                    <h5>{item?.phone?.value}</h5>
                  </Table.Cell>
                </Table.Row>
                <Table.Row>
                  <Table.Cell colSpan="2">
                    {_l`Title`}
                    <h5>{item?.title?.value}</h5>
                  </Table.Cell>
                </Table.Row>
              </Table.Body>
            </Table>
          </div>
        );
      })}

      <br />
      <div className={style.dflex}>
        {isFrom ? <h4 style={{ marginBottom: 0 }}>02 - {_l`From`}</h4> : null}
        {/* If need to add more From User --> open this comment. */}
        {/* {__MODE === 'BUILD' && (
          <Button
            style={{ padding: 5 }}
            size="mini"
            icon={<img src={require('../../../../public/Add.svg')} height={12} />}
            onClick={addFromUser}
          ></Button>
        )} */}
      </div>
      {__ELEMENT_DATA?.__USER_INFO?.map((item, index) => {
        return (
          <div style={{ position: 'relative', marginTop: 5 }} key={index}>
            {__MODE === 'BUILD' && (
              <div>
                <Button
                  style={styleBtnEditInfo}
                  size="mini"
                  icon={<img src={require('../../../../public/Edit.svg')} height={12} />}
                  onClick={() => editUserInfo(index)}
                ></Button>
                <Button
                  style={styleBtnRemoveParty}
                  size="mini"
                  icon={<img src={require('../../../../public/close-task.svg')} height={12} />}
                  onClick={() => deleteUserInfo(index)}
                ></Button>
              </div>
            )}
            <Table celled>
              <Table.Body>
                <Table.Row verticalAlign="top">
                  <Table.Cell width={6}>
                    {_l`Company`}
                    <h5>{item?.companyName?.value}</h5>
                  </Table.Cell>
                  <Table.Cell width={6}>
                    {_l`Name`}
                    <h5>{item?.fullName?.value}</h5>
                  </Table.Cell>
                </Table.Row>
                <Table.Row verticalAlign="top">
                  <Table.Cell width={5}>
                    {_l`VAT number`}
                    <h5>{item?.vatNumber?.value}</h5>
                  </Table.Cell>
                  <Table.Cell width={5}>
                    {_l`Email`}
                    <h5>{item?.email?.value}</h5>
                  </Table.Cell>
                </Table.Row>
                <Table.Row verticalAlign="top">
                  <Table.Cell width={5}>
                    {_l`Address`}
                    <h5>{item?.address?.value}</h5>
                  </Table.Cell>
                  <Table.Cell width={5}>
                    {_l`Phone`}
                    <h5>{item?.phone?.value}</h5>
                  </Table.Cell>
                </Table.Row>
                <Table.Row>
                  <Table.Cell colSpan="2">
                    {_l`Title`}
                    <h5>{item?.title?.value}</h5>
                  </Table.Cell>
                </Table.Row>
              </Table.Body>
            </Table>
          </div>
        );
      })}

      {isHovering && __MODE === 'BUILD' && !isPDF && (
        <div className={style.GroupControllerButton}>
          <GroupControllerButton type={BLOCKTYPE_SIGNATURE.PARTER} index={props.index} />
        </div>
      )}
      {/* <EditParterInfoModal onClose={onCloseModalEdit} visible={visibleModalEdit} formType={editFormType} /> */}

      <ModalCommon
        size="small"
        visible={visibleModalEdit}
        title={_l`Edit information`}
        onDone={onDone}
        onClose={() => onCloseModalEdit()}
      >
        <Grid>
          <GridRow columns={2} width={16}>
            <GridColumn width={8} className={style.dflex}>
              <div className={style.formLabel}>{_l`Company`}</div>
              <div className={style.formField}>
                <Input fluid value={dataEdit?.name?.value} onChange={(e) => updateData('name', e.target.value)} />
              </div>
            </GridColumn>
            {/* <GridColumn width={8} className={style.dflex}>
              <div className={style.formLabel}>{_l`Contact name`}</div>
              <div className={style.formField}>
                <Input
                  fluid
                  value={dataEdit?.fullName?.value}
                  onChange={(e) => updateData('fullName', e.target.value)}
                />
              </div>
            </GridColumn> */}
          </GridRow>
          <GridRow columns={2} width={16}>
            <GridColumn width={8} className={style.dflex}>
              <div className={style.formLabel}>{_l`Contact firstname`}</div>
              <div className={style.formField}>
                <Input
                  fluid
                  value={dataEdit?.firstName?.value}
                  onChange={(e) => updateData('firstName', e.target.value)}
                />
              </div>
            </GridColumn>
            <GridColumn width={8} className={style.dflex}>
              <div className={style.formLabel}>{_l`Contact lastname`}</div>
              <div className={style.formField}>
                <Input
                  fluid
                  value={dataEdit?.lastName?.value}
                  onChange={(e) => updateData('lastName', e.target.value)}
                />
              </div>
            </GridColumn>
          </GridRow>
          <GridRow columns={2} width={16}>
            <GridColumn width={8} className={style.dflex}>
              <div className={style.formLabel}>{_l`VAT number`}</div>
              <div className={style.formField}>
                <Input fluid value={dataEdit?.vat?.value} onChange={(e) => updateData('vat', e.target.value)} />
              </div>
            </GridColumn>
            <GridColumn width={8} className={style.dflex}>
              <div className={style.formLabel}>{_l`Email`}</div>
              <div className={style.formField}>
                <Input fluid value={dataEdit?.email?.value} onChange={(e) => updateData('email', e.target.value)} />
              </div>
            </GridColumn>
          </GridRow>
          <GridRow columns={2} width={16}>
            <GridColumn width={8} className={style.dflex}>
              <div className={style.formLabel}>{_l`Address`}</div>
              <div className={style.formField}>
                <Input fluid value={dataEdit?.address?.value} onChange={(e) => updateData('address', e.target.value)} />
              </div>
            </GridColumn>
            <GridColumn width={8} className={style.dflex}>
              <div className={style.formLabel}>{_l`Phone`}</div>
              <div className={style.formField}>
                <Input fluid value={dataEdit?.phone?.value} onChange={(e) => updateData('phone', e.target.value)} />
              </div>
            </GridColumn>
          </GridRow>
          <GridRow columns={2} width={16}>
            <GridColumn width={8} className={style.dflex}>
              <div className={style.formLabel}>{_l`Title`}</div>
              <div className={style.formField}>
                <Input fluid value={dataEdit?.title?.value} onChange={(e) => updateData('title', e.target.value)} />
              </div>
            </GridColumn>
          </GridRow>
        </Grid>
      </ModalCommon>

      <ModalCommon
        size="tiny"
        title={_l`Add parter`}
        visible={visibleModalAddParter}
        onDone={onDoneModalAddParter}
        onClose={onCloseModalAddParter}
        description={false}
        scrolling={false}
      >
        <div className={style.dflex}>
          <div className={style.formLabel}>{_l`Company`}</div>
          <div className={style.formField}>
            <OrganisationDropdown
              value={dataModalAddParter.companyId}
              onChange={(e, { value }) => {
                setDataModalAddParter({ contactId: null, companyId: value });
              }}
            />
          </div>
        </div>
        <br />
        <div className={style.dflex}>
          <div className={style.formLabel}>
            {_l`Contact`}
            <span style={{ color: 'red' }}>*</span>
          </div>
          <div className={style.formField}>
            <ContactDropdown
              organisationId={dataModalAddParter.companyId}
              value={dataModalAddParter.contactId}
              onChange={(e, { value }) => {
                setDataModalAddParter({ ...dataModalAddParter, contactId: value });
              }}
            />
            {errorContactAddParter && <p style={{ color: '#ed684e', fontSize: '11px' }}>{_l`Contact is required`}</p>}
          </div>
        </div>
      </ModalCommon>
    </div>
  );
};

const mapStateToProps = (state) => {
  const __ELEMENT_DATA = state?.entities?.signature?.__ELEMENTS?.find((e) => e?.type === BLOCKTYPE_SIGNATURE.PARTER);

  return {
    __DEAL: state.entities?.signature?.__DEAL,
    __ELEMENT_DATA,
    __MODE: state.entities?.signature?.__MODE,
    isPDF: state?.entities?.signature?.isShowPDF,
  };
};

const mapDispatchToProps = {
  editParterBlock,
  addMoreParter,
  deleteOneParty,
};

export default connect(mapStateToProps, mapDispatchToProps)(Parties);
