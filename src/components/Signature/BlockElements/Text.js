import React, { useState, useRef, useEffect } from 'react';
import { connect } from 'react-redux';
import style from '../Signature.css';
import _l from 'lib/i18n';
import cx from 'classnames';
import GroupControllerButton from './GroupControllerButton';
import { BLOCKTYPE_SIGNATURE } from '../../../Constants';
import ReactQuill from 'react-quill';
import 'react-quill/dist/quill.bubble.css';
import {
  convertOpsContent,
  editTextBlock,
  editTextBlockInColumnBlock,
  setPositionBlockLastestFocused,
  setDataFillToEditor,
  setEditContent,
  setEditContentInColumnBlock,
} from '../signature.actions';
import moment from 'moment';
import {
  userTags,
  dealTags,
  contactTags,
  companyTags,
  specialTags,
  tagMessages,
  ownerName,
  dayInPipe,
  dateDeal,
  companyCf,
  contactCf,
  dealCf,
} from './tagMessages';

export const Text = (props) => {
  const modules = {
    toolbar: [
      ['bold', 'italic', 'blockquote', { color: [] }, { background: [] }, { header: [1, 2, 3, false] }, { align: [] }],
    ],
  };
  const {
    index,
    __ELEMENTS_DATA,
    editTextBlock,
    editTextBlockInColumnBlock,
    setDataFillToEditor,
    __MODE,
    valueTobeFilledToEditor,
    setPositionBlockLastestFocused,
    positionBlockLastestFocused,
    setEditContent,
    setEditContentInColumnBlock,
    __COMPANY,
    __CONTACT,
    __DEAL,
    __USER_LOGIN_INFO,
    dataCFCompany,
    dataCFContact,
    dataCFDeal,
    inColumnBlock,
    parentColumnIndex,
    parentBlockIndex,
    positionParentBlockLastestFocused,
    positionColumnLastestFocused
  } = props;

  const [isHovering, setIsHovering] = useState(false);
  const refQuill = useRef(null);

  useEffect(() => {
    // console.log(valueTobeFilledToEditor);
    if(inColumnBlock) {
      if (valueTobeFilledToEditor && index === positionBlockLastestFocused && parentColumnIndex === positionColumnLastestFocused && parentBlockIndex === positionParentBlockLastestFocused) {
        const quill = refQuill.current.getEditor();
        quill.focus();
        let range = quill.getSelection();
        let position = range ? range.index : 0;
        console.log('position', position);
        quill.insertText(position, valueTobeFilledToEditor);
      }
    } else {
      if (valueTobeFilledToEditor && index === positionBlockLastestFocused && !positionColumnLastestFocused) {
        const quill = refQuill.current.getEditor();
        quill.focus();
        let range = quill.getSelection();
        let position = range ? range.index : 0;
        console.log('position', position);
        quill.insertText(position, valueTobeFilledToEditor);
      }
    }

  }, [valueTobeFilledToEditor]);

  const [makeMargin, setMakeMargin] = useState(false);
  const onFocusEditor = (range, source, editor) => {
    // console.log(range, source, editor);
    if(inColumnBlock) {
      setPositionBlockLastestFocused(index, parentBlockIndex, parentColumnIndex);

    } else {
      setPositionBlockLastestFocused(index);

    }
    // setMakeMargin(true)
  };

  const onChangeText = (content, delta, source, editor) => {
    if (inColumnBlock) {
      if (__MODE === 'BUILD')
        editTextBlockInColumnBlock(
          parentBlockIndex,
          parentColumnIndex,
          'content',
          content,
          index,
          editor.getContents()
        );
      setEditContentInColumnBlock(parentBlockIndex, parentColumnIndex, content, index);
      setDataFillToEditor('');
    } else {
      editTextBlock('content', content, index, editor.getContents());
      // setEditContent(content, index);
      // setEditContent(content, index, __MODE === 'PREVIEW' ? editor.getContents() : __ELEMENTS_DATA?.pdfToSave)
      setDataFillToEditor('');
    }
  };

  const convertToValue = (text) => {
    let matches = text.match(/\{{(.*?)\}}/g);
    let changeContent = text;
    matches?.forEach((i) => {
      if (companyTags.some((t) => i.slice(0, i.indexOf('_') + 1).includes(t))) {
        let checkIndex = i.slice(i.indexOf('_') + 1, i.length - 2);
        if (specialTags[checkIndex]) {
          let key = __COMPANY?.[specialTags[checkIndex]]?.name;
          changeContent = changeContent.replaceAll(i, key ? key : '');
        } else {
          let key = __COMPANY?.[tagMessages[checkIndex]];
          changeContent = changeContent.replaceAll(i, key ? key : '');
        }
      }
      if (contactTags.some((t) => i.slice(0, i.indexOf('_') + 1).includes(t))) {
        let checkIndex = i.slice(i.indexOf('_') + 1, i.length - 2);
        if (specialTags[checkIndex]) {
          let key = __CONTACT?.[specialTags[checkIndex]]?.name;
          changeContent = changeContent.replaceAll(i, key ? key : '');
        } else {
          let key = __CONTACT?.[tagMessages[checkIndex]];
          changeContent = changeContent.replaceAll(i, key ? key : '');
        }
      }
      if (dealTags.some((t) => i.slice(0, i.indexOf('_') + 1).includes(t))) {
        let checkIndex = i.slice(i.indexOf('_') + 1, i.length - 2);
        if (Object.keys(ownerName).some((t) => checkIndex.includes(t))) {
          let key = __DEAL?.participantList?.[0][ownerName[checkIndex]]?.toString();
          changeContent = changeContent.replaceAll(i, key ? key : '');
        } else {
          // let key = __DEAL?.[checkIndex]
          if (Object.keys(dayInPipe).some((t) => checkIndex.includes(t))) {
            changeContent = changeContent.replaceAll(
              i,
              __DEAL[dayInPipe[checkIndex]]
                ? Math.ceil(__DEAL[dayInPipe[checkIndex]] / 1000 / 3600 / 24)?.toString()
                : ''
            );
          }
          if (Object.keys(dateDeal).some((t) => checkIndex.includes(t))) {
            changeContent = changeContent.replaceAll(
              i,
              __DEAL[dateDeal[checkIndex]]
                ? new moment(__DEAL[dateDeal[checkIndex]]).format('DD-MM-YYYY')?.toString()
                : ''
            );
          } else {
            changeContent = changeContent.replaceAll(
              i,
              __DEAL[tagMessages[checkIndex]] ? __DEAL[tagMessages[checkIndex]] : ''
            );
          }
        }
      }
      if (userTags.some((t) => i.slice(0, i.indexOf('_') + 1).includes(t))) {
        let checkIndex = i.slice(i.indexOf('_') + 1, i.length - 2);
        let key = __USER_LOGIN_INFO?.[tagMessages[checkIndex]];
        changeContent = changeContent.replaceAll(i, key ? key : '');
      }
      if (companyCf.some((t) => i.slice(0, i.indexOf('_') + 1).includes(t))) {
        let thisUuid = i.slice(i.indexOf('_') + 1, i.length - 2);
        let value = dataCFCompany?.find((element) => element.title === thisUuid);
        let key = value.customFieldValueDTOList[0]?.value;
        if (value?.customFieldValueDTOList?.[0]?.value) {
          changeContent = changeContent.replace(i, key);
        } else {
          changeContent = changeContent.replaceAll(i, '');
        }
      }
      if (contactCf.some((t) => i.slice(0, i.indexOf('_') + 1).includes(t))) {
        let thisUuid = i.slice(i.indexOf('_') + 1, i.length - 2);
        let value = dataCFContact?.find((element) => element.title === thisUuid);
        let key = value.customFieldValueDTOList[0]?.value?.toString();
        changeContent = changeContent.replaceAll(i, key ? key : '');
      }
      if (dealCf.some((t) => i.slice(0, i.indexOf('_') + 1).includes(t))) {
        let thisUuid = i.slice(i.indexOf('_') + 1, i.length - 2);
        let value = dataCFDeal?.find((element) => element.title === thisUuid);
        let key = value.customFieldValueDTOList[0]?.value;
        changeContent = changeContent.replaceAll(i, key ? key : '');
      }
    });
    return changeContent;
  };

  // useEffect(() => {
  //   if (__MODE === "PREVIEW") {
  //     let changeContent = convertToValue(__ELEMENTS_DATA?.content)
  //     const quill = refQuill.current.getEditor();
  //     quill.focus();
  //     setEditContent(changeContent, index, quill.getContents())
  //   }
  // }, [__MODE, __ELEMENTS_DATA?.contentTemp,  __ELEMENTS_DATA?.content])

  useEffect(() => {
    let arrOps = __ELEMENTS_DATA?.valuePdf?.ops;
    arrOps?.forEach((i) => {
      i.insert = convertToValue(i.insert);
    });
    convertOpsContent(index, 'valuePdf', { ops: arrOps });
  }, [__ELEMENTS_DATA?.valuePdf]);

  return (
    <div
      className={style.containerElement}
      style={{ display: 'block', padding: inColumnBlock ? 8 : '' }}
      onMouseEnter={() => setIsHovering(true)}
      onMouseLeave={() => setIsHovering(false)}
    >
      {/* <input className={cx(style.inputTextElement)} style={{ fontSize: 24 }} placeholder={_l`Title`} />
      <span role="textbox" contentEditable className={style.textarea} onInput={changeSpan} onBlur={changeSpan}></span> */}
      <div>
        <ReactQuill
          modules={modules}
          onFocus={onFocusEditor}
          readOnly={__MODE === 'PREVIEW'}
          ref={refQuill}
          id="ReactQuillBubble"
          onChange={onChangeText}
          onChangeSelection={(range, source, editor) => {
            if (range && range.length !== 0) setMakeMargin(true);
            else setMakeMargin(false);
          }}
          // onKeyPress={() => setMakeMargin(false)}
          style={{ height: 'fit-content', marginBottom: makeMargin && __MODE !== 'PREVIEW' ? 120 : 0 }}
          value={__ELEMENTS_DATA?.content || ''}
          // value={__MODE === 'PREVIEW' ? __ELEMENTS_DATA?.contentTemp : __ELEMENTS_DATA?.content || ''}
          className={style.reactQuill}
          theme="bubble"
          onBlur={() => setMakeMargin(false)}
          placeholder={_l`Title`}
          scrollingContainer="#grid-column-main-layout-signature"
        />
      </div>

      {isHovering && __MODE === 'BUILD' && (
        <div className={style.GroupControllerButton}>
          <GroupControllerButton
            type={BLOCKTYPE_SIGNATURE.TEXT}
            index={props.index}
            inColumnBlock={inColumnBlock}
            parentColumnIndex={parentColumnIndex}
            parentBlockIndex={parentBlockIndex}
          />
        </div>
      )}
    </div>
  );
};

const mapStateToProps = (state, { index, inColumnBlock, parentColumnIndex, parentBlockIndex }) => {
  let __ELEMENTS = state?.entities?.signature?.__ELEMENTS;
  if (inColumnBlock) {
    __ELEMENTS = state?.entities?.signature?.__ELEMENTS?.[parentBlockIndex]?.__ELEMENTS?.[parentColumnIndex];
  }
  return {
    __ELEMENTS_DATA: __ELEMENTS[index],
    __MODE: state?.entities?.signature?.__MODE,
    valueTobeFilledToEditor: state?.entities?.signature?.valueTobeFilledToEditor,
    positionBlockLastestFocused: state?.entities?.signature?.positionBlockLastestFocused,
    positionParentBlockLastestFocused: state?.entities?.signature?.positionParentBlockLastestFocused,
    positionColumnLastestFocused: state?.entities?.signature?.positionColumnLastestFocused,
    __DEAL: state?.entities?.signature?.__DEAL,
    __COMPANY: state?.entities?.signature?.__COMPANY,
    __CONTACT: state?.entities?.signature?.__CONTACT,
    __USER_LOGIN_INFO: state?.entities?.signature?.__USER_LOGIN_INFO,
  };
};

const mapDispatchToProps = {
  editTextBlock,
  editTextBlockInColumnBlock,
  setPositionBlockLastestFocused,
  setDataFillToEditor,
  setEditContent,
  setEditContentInColumnBlock,
  convertOpsContent,
};

export default connect(mapStateToProps, mapDispatchToProps)(Text);
