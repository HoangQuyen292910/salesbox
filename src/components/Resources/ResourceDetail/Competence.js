import React, { useState } from 'react';
import { Dropdown, Grid, Input, GridColumn, GridRow, Icon, Button, Popup } from 'semantic-ui-react';
import { RESOURCE_TAB } from '../../../Constants';
import styles from './Competence.css';
import css from './Experiences/Experiences.css';
import _l from 'lib/i18n';
import ModalCommon from '../../ModalCommon/ModalCommon';
import { connect } from 'react-redux';
import api from 'lib/apiClient';
import { getAllCompetence, updateCompetenceItem } from '../resources.actions';

const Competence = ({
  item,
  lastUsedOption,
  deleteCompetenceItem,
  index,
  levelOptions,
  conpetencesName,
  updateSingleCompetence,
  resourceId,
  tab,
  getAllCompetence,
  showUpdateCompetence,
  updateCompetenceItem
}: any) => {
  const handleDeleteCompetenceItem = () => {
    deleteCompetenceItem && deleteCompetenceItem(index);
  };
  const handleChangeDropdown = (key, data) => {
    item[key] = data.value;
    updateSingleCompetence && updateSingleCompetence([item], resourceId);
  };

  const onShowUpdateCompetence = () => {
    // setshowPopupUpdateCompetence(true);
    // setCompetenceNameUpdate(item.competenceName);
    updateCompetenceItem({
      uuid: item.uuid,
      competenceLevel: item.competenceLevel,
      competenceId: item.competenceId,
      lastUsed: item.lastUsed,
    });
    showUpdateCompetence();

  };

  const [error, setError] = useState(null);
  const onCloseUpdateCompetence = () => {
    setCompetenceNameUpdate('');
    setshowPopupUpdateCompetence(false);
    setError(null);
  };
  const onDoneUpdateCompetence = async () => {
    if (competenceNameUpdate?.trim() === '' || !competenceNameUpdate) {
      setError(_l`Name is required`);
      return;
    }
    try {
      const res = await api.post({
        resource: `consultant-v3.0/competence/update`,
        query: {
          uuid: item.competenceId,
        },
        data: {
          name: competenceNameUpdate,
          languageVersion: localStorage.getItem('language') || 'en',
        },
      });
      if (res) {
        getAllCompetence(localStorage.getItem('language') || 'en');
        setCompetenceNameUpdate('');
        setshowPopupUpdateCompetence(false);
        setError(null);
      }
    } catch (error) {
      setError(_l`Name already exist.`);
    }
  };
  const [competenceNameUpdate, setCompetenceNameUpdate] = useState('');
  const [showPopupUpdateCompetence, setshowPopupUpdateCompetence] = useState(false);
  return (
    <div className={styles.competenceItem} key={index}>
      <Grid>
        <GridRow>
          <GridColumn width={2} className={styles.clearPaddingRight}>
            <Dropdown
              placeholder="Level"
              value={item.competenceLevel}
              onChange={(e, val) => {
                handleChangeDropdown('competenceLevel', val);
              }}
              fluid
              selection
              search
              options={levelOptions}
            />
          </GridColumn>
          <GridColumn width={tab === RESOURCE_TAB.CV ? 6 : 6} className={styles.clearPaddingRight}>
            <Dropdown
              placeholder="Select competence"
              value={item.competenceId}
              onChange={(e, val) => {
                handleChangeDropdown('competenceId', val);
              }}
              search
              fluid
              selection
              options={conpetencesName}
            />
          </GridColumn>
          <GridColumn width={5} className={styles.clearPaddingRight}>
            <Dropdown
              placeholder="Select last used"
              search
              fluid
              selection
              options={lastUsedOption}
              onChange={(e, val) => {
                handleChangeDropdown('lastUsed', val);
              }}
              value={item.lastUsed}
            />
          </GridColumn>
          <GridColumn width={3}>
            {tab === RESOURCE_TAB.CV ? (
              <>
                <Icon name="check" circular color="grey" className={css.iconListItem} />
              </>
            ) : (
              <div className="actions" style={{ display: 'flex' }}>
                {/* <Icon circular className={styles.action} color="grey" name="list ul"></Icon>
                <Icon circular className={styles.action} color="grey" name="close" onClick={handleDeleteCompetenceItem}></Icon> */}

                <Popup
                  trigger={
                    <div className={css.iconList}>
                      <Icon name="list ul" color="grey" className={css.iconItem} />
                    </div>
                  }
                  hoverable
                  content={{ content: <p style={{ fontSize: 11 }}>{_l`Drag & Drop`}</p> }}
                />
                <Popup
                  trigger={
                    <Button
                      style={{ backgroundColor: '#f0f0f0' }}
                      icon={<img src={require('../../../../public/Edit.svg')} height={11} />}
                      size="mini"
                      className={css.deleteButton}
                      onClick={onShowUpdateCompetence}
                      circular
                      compact
                    />
                  }
                  hoverable
                  content={{ content: <p style={{ fontSize: 11 }}>{_l`Update`}</p> }}
                />
                <Popup
                  trigger={
                    <Button
                      icon="close"
                      size="mini"
                      onClick={handleDeleteCompetenceItem}
                      circular
                      compact
                      style={{ backgroundColor: '#f0f0f0' }}
                    />
                  }
                  hoverable
                  content={{ content: <p style={{ fontSize: 11 }}>{_l`Delete`}</p> }}
                />
              </div>
            )}
          </GridColumn>
        </GridRow>
      </Grid>

      <ModalCommon
        title={_l`Update competence`}
        visible={showPopupUpdateCompetence}
        size="tiny"
        onClose={onCloseUpdateCompetence}
        onDone={onDoneUpdateCompetence}
      >
        <Grid>
          <Grid.Row>
            <Grid.Column width="4" verticalAlign="middle">
              <b>
                {_l`Competence`} <span style={{ color: 'red' }}>*</span>
              </b>
            </Grid.Column>
            <Grid.Column width="12">
              <Input
                fluid
                value={competenceNameUpdate}
                error={error !== null}
                onChange={(e) => setCompetenceNameUpdate(e.target.value)}
              ></Input>
              {error && <p style={{ color: 'red' }}> {error}</p>}
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </ModalCommon>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {};
};
const mapDispatchToProps = {
  getAllCompetence,
  updateCompetenceItem
};
export default connect(mapStateToProps, mapDispatchToProps)(Competence);
