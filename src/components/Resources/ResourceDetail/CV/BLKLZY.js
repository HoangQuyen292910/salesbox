import React from 'react';
// import moment from 'moment';
import { Page, Text, View, Document, StyleSheet, Image, Font } from '@react-pdf/renderer';
// import Open_Sans from "../../../../font/Open_Sans/OpenSans-Regular.ttf"
// import Open_Sans_Bold from "../../../../font/Open_Sans/OpenSans-Bold.ttf"
import Mabry_Pro from "../../../../font/Mabry_Pro/mabry-regular-pro.ttf";
import Mabry_Pro_Bold from "../../../../font/Mabry_Pro/mabry-bold-pro.ttf";
import logo from '../../../../../public/001.png';
import userLogo from '../../../../../public/userGray.png';

Font.register({
  family: 'Mabry_Pro',
  src: Mabry_Pro
})

Font.register({
  family: 'Mabry_Pro_Bold',
  src: Mabry_Pro_Bold
})
// Font.register({
//   family: 'Oswald',
//   src: 'https://fonts.gstatic.com/s/oswald/v13/Y_TKV6o8WovbUd3m_X9aAA.ttf'
// });


Font.registerHyphenationCallback(word => (
  [word]
));
const LENGTH_TEXT = 1000;
const styles = StyleSheet.create({
  page: {backgroundColor: '#FBFBFB', fontSize: 9, fontFamily: "Mabry_Pro", color: '#100C08'},
  leftH: {width: "30%"},
  centerH: {width: "40%", textAlign: 'left'},
  rightH: {width: "30%", textAlign: 'center'},
  nameH: {textAlign: 'center', fontWeight: '400', fontSize: '25'},
  textCenter: {textAlign: 'center'},
  avatar: { width: "351px", height: '351px' },
  avatarC: {padding: "40px 20px"},
  bulletPoint: {
    width: 7,
    height: 7,
    backgroundColor: "#000",
    fontSize: 20,
    marginRight: "7",
    marginTop: "4"
  },
  inline: {flexDirection: 'row'},
  consultantN: { textAlign:'center', fontSize: '30', marginTop: '20'},
  newspaper: {
    flexDirection: 'row',
    paddingLeft: 122
  },
  section: {
    width: 229,
    // padding: 10,
    // flexGrow: 1
  },
  header: { marginBottom: '70'},
  textHighlight: { fontSize: '20', textTransform: "uppercase" },
  nameHighlight: {fontSize: '75', maxLines: "2"},
  consultant: {paddingLeft: 122, fontSize: '30', margin: '10px 0'},
  row: {flexDirection: 'row', marginBottom: 10},
  col4One: {width: 107},
  col4Two: {width: 228, margin: '0 15px'},
  col4Three: {width: 107},
  col4Four: {width: 107, marginLeft: 15},
  col3One: {width: 107},
  col3Two: {width: 228, margin: '0 15px' },
  col3Three: {width: 228},
  fontBold: {fontFamily: "Mabry_Pro_Bold"},
  titlePage3: {width: '20%', textTransform: 'uppercase', fontSize: 9},
  footer: {
    flexDirection: 'row',
    marginTop: 30,
    paddingLeft: '45%',
  },
});

const BLKLZY = ({
  _l,
  profileDetail,
	company,
  user,
  competenceCheck,
  header,
  experienceCheck,
  experienceList,
  listEmployee,
  listEducation,
  listLanguage,
  listCertificate,
  moment,
  title,
  description
}) => {

  const translateValueLeveLang = (level) => {
    switch(level) {
      case 'GOOD':
        return _l`Good`;
      case 'NATIVE':
        return _l`Native`;
      case 'FLUENT':
        return _l`Fluent`;
      case 'BASIC':
        return _l`Basic`;
    }
  }
  return (
<Document>
    <Page size="A4" style={styles.page}>
      <View style={styles.header} fixed>
      <View style={{flexDirection: 'row'}}>
        <View style={styles.leftH}>
          <Image style={{height: 15, width: 36, marginLeft: 122}}
            src={logo}
          />
        </View>
        <View fixed style={{ position: 'absolute', left: 244, width: '40%'}}>
          <View>
          <Text>CV</Text>
          <Text style={styles.fontBold}>{`${profileDetail?.firstName} ${profileDetail?.lastName}`}</Text>
          </View>
        </View>
        <Text style={{ position: 'absolute', left: 487 }} render={({ pageNumber, totalPages }) => (
          `${_l`Page`} ${pageNumber} / ${totalPages}`
        )} fixed />
      </View>
      </View>

      <Text style={styles.nameHighlight} >{`${profileDetail?.firstName} ${profileDetail?.lastName}`}</Text>
      <View style={{flexDirection: 'row'}}>
        <View style={styles.avatar}>
            <Image style={{width: "100%"}} cache={false} source={{ uri: profileDetail?.avatar ? `https://d3si3omi71glok.cloudfront.net/salesboxfiles/${profileDetail?.avatar.substr(profileDetail?.avatar.length - 3)}/${profileDetail?.avatar}` : userLogo, method: "GET", headers: { "Cache-Control": "no-cache" } }}></Image>

        </View>
        <View style={styles.avatarC}>
          {competenceCheck?.length > 0 ?
            <>
              <View style={{margin: "20px 0"}}>
                <Text style={{ textTransform: "uppercase" }}>{header ? header : _l`Competences`}</Text>
                {competenceCheck.map((item) => {
                  return (
                    <View style={[styles.inline, { width: 230}]}>
                      <Text style={styles.bulletPoint}></Text>
                      <Text style={styles.fontBold}>{item.competenceName}</Text>
                    </View>
                  )
                })}
              </View>
            </> : null
          }
          <View>
          {experienceCheck?.length > 0 ? <>
        	<Text style={{ textTransform: "uppercase" }}>{_l`Key assignments`}</Text>
            {experienceCheck.map((item, index) => {
                return index < 5 ? (
                  <View style={[styles.inline, { width: 230}]}>
                  <Text style={styles.bulletPoint}></Text>
                  <Text style={styles.fontBold}>{`${item.title}/ ${item.company}`}</Text>
                </View> ) : null
              })}
            </> : null }
          	</View>
        </View>
      </View>
      <Text style={styles.consultant}>{title || profileDetail?.title}</Text>
      	<View style={styles.newspaper}>
            <View style={[styles.section, { marginRight: 13 }]}>
              <Text>{description?.substring(0, description.substring(LENGTH_TEXT, description.length).indexOf(" ") + LENGTH_TEXT) || profileDetail?.profileDescription?.substring(0, profileDetail?.profileDescription.substring(LENGTH_TEXT, profileDetail?.profileDescription.length).indexOf(" ") + LENGTH_TEXT)}</Text>
            </View>
            <View style={styles.section}>
              <Text>{description?.substring(description.substring(LENGTH_TEXT, description.length).indexOf(" ") + LENGTH_TEXT +1, LENGTH_TEXT * 2) || profileDetail?.profileDescription?.substring(profileDetail?.profileDescription.substring(LENGTH_TEXT, profileDetail?.profileDescription.length).indexOf(" ") + LENGTH_TEXT +1, LENGTH_TEXT * 2)} {description?.length > 2000 || profileDetail?.profileDescription?.length > 2000 ? '...' : ''}</Text>
            </View>
        </View>
    </Page>
    <Page size="A4" style={styles.page}>
    <View style={styles.header} fixed>
      <View style={{flexDirection: 'row'}}>
        <View style={styles.leftH}>
          <Image style={{height: 15, width: 36, marginLeft: 122}}
            src={logo}
          />
        </View>
        <View fixed style={{ position: 'absolute', left: 244, width: '40%'}}>
          <View>
          <Text>CV</Text>
          <Text style={styles.fontBold}>{`${profileDetail?.firstName} ${profileDetail?.lastName}`}</Text>
          </View>
        </View>
        <Text style={{ position: 'absolute', left: 487 }} render={({ pageNumber, totalPages }) => (
          `${_l`Page`} ${pageNumber} / ${totalPages}`
        )} fixed />
      </View>
      </View>
      {experienceList?.length > 0 ? (
        <>
          <Text style={styles.nameHighlight}>{_l`Client Assignments`}</Text>
          {experienceList.map((item, index) => {
          return (
            <View wrap={false}>
          <View style={styles.col4One}>
                <Text style={{fontFamily: "Mabry_Pro_Bold", fontSize: '12'}}>{item.company}</Text>
          </View>
          <View style={styles.row}>
          <View style={styles.col4One}>
              </View>
              <View style={styles.col4Two}>
                  <Text>{item.description}</Text>
              </View>
              <View style={styles.col4Three}>
                  <View style={{marginBottom: '20px'}}>
                    <Text style={{textTransform: "uppercase"}}>{_l`Role`}:</Text>
                    <Text style={styles.fontBold}>{item.title}</Text>
                </View>
                <View>
                  <Text style={{textTransform: "uppercase"}}>{_l`Period`}:</Text>
                  <Text style={styles.fontBold}> {moment(item.startDate).format('MMM YYYY')} - {item.endDate ? moment(item.endDate).format('MMM YYYY') : _l`Present`}</Text>
                </View>
              </View>
              <View style={styles.col4Four}>
                <Text>{_l`Competences`}</Text>
                {
                  item.competenceDTOList.map((com) => {
                      return (
                        <View style={[styles.inline, { width: 91}]}>
                          <Text style={styles.bulletPoint}></Text>
                          <Text key={`exx-${com.uuid}`} style={styles.fontBold}>{com.competenceName}</Text>
                        </View>
                      )
                    })
                  }
              </View>
          </View>
          </View>
          )})}
          </>) : null
      }
  	</Page>

  	<Page size="A4" style={styles.page}>
    <View style={styles.header} fixed>
      <View style={{flexDirection: 'row'}}>
        <View style={styles.leftH}>
          <Image style={{height: 15, width: 36, marginLeft: 122}}
            src={logo}
          />
        </View>
        <View fixed style={{ position: 'absolute', left: 244, width: '40%'}}>
          <View>
          <Text>CV</Text>
          <Text style={styles.fontBold}>{`${profileDetail?.firstName} ${profileDetail?.lastName}`}</Text>
          </View>
        </View>
        <Text style={{ position: 'absolute', left: 487 }} render={({ pageNumber, totalPages }) => (
          `${_l`Page`} ${pageNumber} / ${totalPages}`
        )} fixed />
      </View>
      </View>
      {listEmployee?.length > 0 ? (
        <View>
            <Text style={styles.titlePage3}>{_l`Employer`}</Text>
            {listEmployee.map((item, index) => (
            <View style={styles.row} wrap={false}>
              <View style={styles.col4One}>
              </View>
                <View style={styles.col4Two}>
                    <Text style={styles.fontBold}>{item.name}</Text>
                </View>
                <View style={styles.col4Three}>
                    <Text>{item.startYear} - {item.endYear === 999999 ? _l`Present` : item.endYear}</Text>
                </View>
                <View style={styles.col4Four}>
                </View>
            </View>
            ))}
        </View>
      ) : null }

        {listEducation?.length > 0 ? (
        <View>
            <Text style={styles.titlePage3}>{_l`Education`}</Text>
            {listEducation.map((item, index) => (
            <View style={styles.row} wrap={false}>
              <View style={styles.col4One}>
              </View>
                <View style={styles.col4Two}>
                  <Text style={styles.fontBold}>{item.name}</Text>
                </View>
                <View style={styles.col4Three}>
                  <Text>{item.startYear} - {item.endYear}</Text>
                </View>
                <View style={styles.col4Four}>
                  <Text style={styles.fontBold}>{item.school}</Text>
                </View>
            </View>
            ))}
        </View>
      ) : null }

      {listCertificate?.length > 0 ? (
        <View>
            <Text style={styles.titlePage3}>{_l`Courses and Certificates`}</Text>
            {listCertificate.map((item, index) => (
            <View style={styles.row} wrap={false}>
              <View style={styles.col4One}>
              </View>
                <View style={styles.col4Two}>
                    <Text style={styles.fontBold}>{item.name}</Text>
                </View>
                <View style={styles.col4Three}>
                    <Text>{item.year}</Text>
                </View>
                <View style={styles.col4Four}>
                </View>
            </View>
            ))}
        </View>
      ) : null }

      {listLanguage?.length > 0 ? (
        <View>
            <Text style={styles.titlePage3}>{_l`Language`}</Text>
            {listLanguage.map((item, index) => (
            <View style={styles.row} wrap={false}>
              <View style={styles.col4One}>
              </View>
                <View style={styles.col4Two}>
                    <Text style={styles.fontBold}>{item.name}</Text>
                </View>
                <View style={styles.col4Three}>
                    <Text>{translateValueLeveLang(item.level)}</Text>
                </View>
                <View style={styles.col4Four}>
                </View>
            </View>
            ))}
        </View>
      ) : null }

      <View style={styles.footer}>
        <View>
        	<Text style={{textTransform: "uppercase"}}>{_l`Sales contact`}:</Text>
        </View>
        <View style={{paddingLeft: '45'}}>
        	<Text style={{padding: "20 0", fontFamily: "Mabry_Pro_Bold"}}>{user?.name}</Text>
          	<Text>{_l`Write`}: {user?.email}</Text>
          	<Text>{_l`Talk`}: {user?.phone}</Text>
        </View>
    	</View>
  	</Page>
  </Document>
  );
}

export default BLKLZY;
