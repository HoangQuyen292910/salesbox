import React from 'react';
import { Dropdown } from 'semantic-ui-react';
import _l from 'lib/i18n';

function YearDropdown(props) {
  const { hasCurrentOption } = props;
  const { calculatingPositionMenuDropdown, colId, _class, ...other } = props;
  let currentYear = new Date().getFullYear();
  let options = [];
  for (let i = 0; i <= 50; i++) {
    options.push({ key: currentYear - i, text: currentYear - i, value: currentYear - i });
  }
  if (hasCurrentOption) {
    options = [{ key: 'Current', text: _l`Current`, value: 999999 }, ...options];
  }
  return (
    <Dropdown
      id={colId}
      className={_class}
      options={options}
      onClick={() => {
        calculatingPositionMenuDropdown && calculatingPositionMenuDropdown(colId);
      }}
      fluid
      selection
      search
      {...other}
    />
  );
}

export default YearDropdown;
